#include "ast_tree.h"
#include "tabela_simbolos.h"

int check_void_var_declaration(Node n);

int check_void_in_param_func_dec(Node n);

int check_conflicting_type(Node n, elementoTabela *existe );

int check_repeated_vars_in_func_def(Node paramtype);