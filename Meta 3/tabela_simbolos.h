#include "ast_tree.h"

#ifndef tabelas
#define tabelas

typedef struct elementoTabela
{
	char *name;
	char *type;
	char *param;
	int numParam;						// se funcao => numParam > 0
	int definido;						// 1->sim 0->nao
	struct elementoTabela *next;

} elementoTabela;

typedef struct tabela
{
	char *name;
	int definido;
	elementoTabela *first_element;
	struct tabela *next;
} tabela;

#endif

void printGlobalTable(tabela *t);
void printLocalTable(tabela *t);
void printTables(tabela *t);

tabela* init_symbol_table();
void freeTables(tabela *t);
void freeElements(elementoTabela *e);

void check_program(Node root);
void addAnnotation(Node n, char *str);

elementoTabela* find_node(Node n, tabela *tab);

tabela* addTable(tabela *g, Node n, elementoTabela *e);
int init_table(tabela *t, Node n);
tabela* getTabela(tabela *g, char* name);
elementoTabela *getNextFunction(elementoTabela *e);

void check_body(Node n, tabela *t, tabela *g);

int typeCompare(char *type1, char *type2);
int numParamNode(Node n);

int isTerminal(Node n) ;

char *getType(Node n);

void removeElementoTabela(elementoTabela *e, tabela *t);
