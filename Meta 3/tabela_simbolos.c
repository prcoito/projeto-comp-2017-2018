#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tabela_simbolos.h"
#include "ast_tree.h"
#include "erros.h"
#include <ctype.h>

extern int NOT_ERRO;

void printTables(tabela *t){
	printGlobalTable(t);

	t = t->next;
	while(t!=NULL){
		if(t->definido)
			printLocalTable(t);
		t = t->next;
	}
}

void printGlobalTable(tabela *t){
	printf("===== Global Symbol Table =====\n");
	elementoTabela *e = t->first_element;
	while(e!=NULL){
		printf("%s\t%s", e->name, e->type);
		if(e->param){
			printf("\t%s", e->param);
		}
		printf("\n");
		e = e->next;
	}
	printf("\n");
}

void printLocalTable(tabela *t){
	printf("===== Function %s Symbol Table =====\n", t->name);
	elementoTabela *e = t->first_element;
	while(e!=NULL){
		printf("%s\t%s", e->name, e->type);
		if(e->param){
			printf("\t%s", e->param);
		}
		printf("\n");
		e = e->next;
	}
	printf("\n");
}

tabela* init_symbol_table(){
	tabela *global = (tabela *)malloc(sizeof(tabela));
	elementoTabela *put = (elementoTabela *)malloc(sizeof(elementoTabela));
	elementoTabela *get = (elementoTabela *)malloc(sizeof(elementoTabela));

	global->name = NULL;
	global->first_element = put;
	global->next = NULL;

	put->name = (char *)malloc( (strlen("putchar") + 1) * sizeof(char) );
	strcpy(put->name, "putchar");
	put->type = (char *)malloc( (strlen("int(int)") + 1) * sizeof(char) );
	strcpy(put->type, "int(int)");
	put->numParam = 1;
	put->definido = 1;
	put->next = get;

	get->name = (char *)malloc( (strlen("getchar") + 1) * sizeof(char) );
	strcpy(get->name, "getchar");
	get->type = (char *)malloc( (strlen("int(void)") + 1) * sizeof(char) );
	strcpy(get->type, "int(void)");
	put->numParam = 1;
	get->definido = 1;
	get->next = NULL;

	return global;
}

void addAnnotation(Node n, char *str){
	free(n->annotation); // para o strdup anterior
	n->annotation = strdup(str);

	return;
}

int typeCheckFunc(Node n, elementoTabela *e){
	//printf("e->type %s\n", e->type);
	//print_tree(n,1);
	
	char tempE[100];
	strcpy(tempE, e->type);
	
	Node aux = n->child;
	char *p = strtok(tempE, "(");
	//p[0] = toupper(p[0]);
	// CHECK O TIPO DE RETORNO
	/*printf("strcmp(%s ,%s) = %d\n",p, aux->type, strcmp(p,aux->type));
	if( strcmp(p, aux->type)!=0 )
	{
		printf("Line %d,col %d: Conflicting types (got %s, expected %s)\n", aux->numLinhas,
																			aux->numColunas,
																			p, aux->type);
		while(1){;}
		return 0;
	}
	while(1){;}*/
	aux = aux->brother->brother->child;
	p = strtok(NULL, ",)");
	while( p && aux->child ){
		p[0] = tolower(p[0]);
		aux->child->type[0] = tolower(aux->child->type[0]);
		
		if( strcmp(p,aux->child->type)!=0 )
		{
			printf("Line %d, col %d: Conflicting types (got %s, expected %s)\n", aux->child->numLinhas  ,
																				aux->child->numColunas - (int)strlen(aux->child->type) ,
																				aux->child->type, p);
			//while(1){;}
			return 0;
		}
		p = strtok(NULL, ",)");
		aux->child->type[0] = toupper(aux->child->type[0]);
	}
	if ( ( p && aux->child == NULL ) || ( aux->child && p == NULL) )
		return 0;
	
	return 1;
}

// FUNCOES CABECALHOS
int check_declarator(Node n, tabela* global){
	//char *temp;
	Node paramtype, check_void;
	//int i = 0, j=0;
	//int numParam = 0;
	//printf("n->child->brother %s\n", n->child->brother->value);
	//#ver se ja existe
	elementoTabela *existe=find_node(n->child->brother, global);
	//printf("existe === NULL (%d)\n", existe == NULL);
	//se ja existe
	if(existe){
		
		//DEFAULT: nao fazer nada
	
		//#########OPCIONAL###########
		//# compara parametros
		
		//se forem iguais
			//nao faz nada
		//senao
			//da erro
		check_void = n->child->brother->brother->child;
		/*
		if ( check_void_in_param_func_dec(check_void) ){
			return -1;
		}
		*/
		paramtype=n->child->brother->brother->child;
		if ( check_repeated_vars_in_func_def(paramtype) ){
			return -1;
		}

		if ( check_conflicting_type(n, existe) ){
			return -1;
		}
		
		/*
		if ( n->child->brother->brother != NULL )
			numParam = numParamNode(n->child->brother->brother);

		if ( existe->numParam != numParam ){
			printf("Line %d, col %d: Wrong number of arguments to function %s (got %d, required %d)\n",  n->numLinhas, n->numColunas,
																										n->child->brother->value,
																									 	numParam, existe->numParam);
			return -1;
		}
		*/

		// check if repeat vaŕs
		
		
	}
	//senao
	else{

		check_void = n->child->brother->brother->child;
		
		/*if ( check_void_in_param_func_dec(check_void) ){
			return -1;
		}*/
		
		paramtype=n->child->brother->brother->child;
		if ( check_repeated_vars_in_func_def(paramtype) ){	// tambem deteta voids
			return -1;
		}

		//pode adicionar a vontade
		elementoTabela *aux= global->first_element;
		while(aux->next!=NULL){
			aux=aux->next;
		}

		elementoTabela *dec= (elementoTabela*)malloc(sizeof(elementoTabela));
		
		dec->name=strdup(n->child->brother->value);
		dec->type= strdup(n->child->type);
		dec->type=(char*)realloc(dec->type,(128*6+1)*sizeof(char));
		dec->numParam = 1;																
		strcat(dec->type,"(");
		
		Node paramtype=n->child->brother->brother->child;
		strcat(dec->type,paramtype->child->type);
		paramtype= paramtype->brother;
		
		//int count=1;
		
		while(paramtype){
			strcat(dec->type,",");
			strcat(dec->type,paramtype->child->type);
			paramtype= paramtype->brother;
			dec->numParam = dec->numParam+1;
			
		
		}
		strcat(dec->type,")");
		
		for(int i = 0; dec->type[i]; i++){
  			dec->type[i] = tolower(dec->type[i]);
		}
		
		dec->next=NULL;
		
		aux->next=dec;

		//printf("VOU adicionar tabela\n");
		tabela *t = addTable(global, n, NULL);
		//printf("t == NULL %d\n", t==NULL);
		t->definido = 0;
		
	}
	return 0;
}

// VARIVEIS DECLARACAO
int check_declaration( Node n, tabela *t, tabela *global ){
	//printf("n->name = %s\n", n->child->brother->value);
	//Node current;
	elementoTabela *existe=NULL;
	//#ver se ja existe
	if ( t!=NULL ) {
		existe=find_node(n->child->brother, t);
	}
	else{
		existe=find_node(n->child->brother, global);
		
	}
	
	if ( existe ){															// ver se já esta definido
		//printf("%s %s\n", existe->type, n->child->type);

		existe->type[0] = toupper(existe->type[0]);
		if ( existe->definido || strcmp(existe->type, n->child->type) != 0){
			existe->type[0] = tolower(existe->type[0]);
			printf("Line %d, col %d: Symbol %s already defined\n", n->child->brother->numLinhas, 
																  n->child->brother->numColunas - (int)strlen(n->child->brother->value),
																  existe->name);

		}
		
		existe->type[0] = tolower(existe->type[0]);
		
		if ( n->child->brother->brother != NULL ){
			check_body(n->child->brother->brother, t, global);	
			existe->definido = 1;

			if ( n->child->brother->brother &&
				 n->child->brother->brother->annotation && 
				 strcmp(n->child->brother->brother->annotation, "double") == 0){
				if ( strcmp(existe->type, "double" ) != 0)
				{
					printf("Line %d, col %d: Conflicting types (got %s, expected %s)\n", n->child->brother->numLinhas, n->child->brother->numColunas -(int)strlen(n->child->brother->value),
																					     n->child->brother->brother->annotation ,
																					 	 existe->type);

				}

			}

		}
		//}
		//existe->type[0] = tolower(existe->type[0]);
	}
			
	/*
	//se ja existe
	if(existe){
		// DEFAULT: erro
		//;
		//#########OPCIONAL###########
		//# ve se ja esta definido
		// VERIFICAR SE ESTA BEM
		if ( existe->definido )
			printf("Line %d,col %d: Symbol %s already defined\n", n->numLinhas, n->numColunas, existe->name);
			NOT_ERRO = 0;
																													// <---- Agora resta perceber o q fazer na arvore???
																													// meter UNDEF????
	
	//senao
	}
	
	*/
	else{
		// ver se type é void
		//print_tree(n->child, 10);
		// printf("########################\n");
		
		if ( check_void_var_declaration(n->child) ){
			return -1;
		}
		

		elementoTabela *aux= global->first_element;
		//pode adicionar a vontade
		if (t==NULL){
			aux= global->first_element;
		}
		else{
			aux= t->first_element;
		}
		
		while(aux->next!=NULL){
			aux=aux->next;
		}
		
		

		elementoTabela *dec= (elementoTabela*)malloc(sizeof(elementoTabela));
		
		dec->name= strdup(n->child->brother->value);
		dec->type= strdup(n->child->type);
		dec->numParam = 0;

		for(int i = 0; dec->type[i]; i++){
  			dec->type[i] = tolower(dec->type[i]);
		}
		
		
		dec->next=NULL;
		dec->definido = 0;
		aux->next=dec;

		/* adicionei  VERIFICAR SE È ASSIM */  									// <-------- Estas a tomar a decis? de definir a variavel??
		if ( n->child->brother->brother != NULL ){
			check_body(n->child->brother->brother, t, global);
			dec->definido = 1;

			//current = n->child->brother->brother;

			//dec->type[0] = tolower(dec->type[0]);
			
			if ( n->child->brother->brother &&
				 n->child->brother->brother->annotation && 
				 strcmp(n->child->brother->brother->annotation, "double") == 0){
				if ( strcmp(dec->type, "double" ) != 0)
				{
					printf("Line %d, col %d: Conflicting types (got %s, expected %s)\n", n->child->brother->numLinhas, n->child->brother->numColunas -(int)strlen(n->child->brother->value),
																					     n->child->brother->brother->annotation ,
																					 	 dec->type);

				}

			}
			//current->child->type[0] = toupper(current->child->type[0]);
		}
		
	}
	return 0;
}

// IMPLEMENTACAO
int check_definition( Node n, tabela *global ){
	//char *temp;
	//int numParam = 0;
	//printf("n->name = %s\n", n->child->brother->value);

	//#ver se ja existe
	elementoTabela *existe=find_node(n->child->brother, global);
	//printf("existe == %d\n", existe == NULL);
	//se ja existe
	if(existe){
		/*if ( n->child->brother->brother != NULL )
			numParam = numParamNode(n->child->brother->brother);
		//# ve se ja esta definido
		*/
		if(existe->definido){
			//SE SIM
				//ERRO
			printf("Line %d, col %d: Symbol %s already defined\n", 	n->child->brother->numLinhas, 
																	n->child->brother->numColunas - (int)strlen(n->child->brother->value), 
																	existe->name);
			NOT_ERRO = 0;
		}

		/*else if( existe->numParam != numParam ) {			// nº parametros !=
			printf("Line %d,col %d: Wrong number of arguments to function %s (got %d, required %d)\n",  n->numLinhas, n->numColunas,
																										n->child->brother->value,
																									 	numParam, existe->numParam);
		}*/
		else{
			//SENAO

			if ( check_repeated_vars_in_func_def(n->child->brother->brother->child) )
				return -1;
			
			if ( check_conflicting_type(n, existe) ){
				return -1;
			}

			existe->definido = 1;
			//printf("existe->name %s next = %s\n", existe->name, getNextFunction(existe)->name);
			//tabela *t = addTable(global, n, getNextFunction(existe));
			tabela *t = getTabela(global, existe->name);
			//printf("tabela == NULL (%d)\n", t==NULL);
			
			if ( init_table(t, n) == -1)
			{
				//removeElementoTabela(existe, global);
				//return -1;
			}

			t->definido = 1;
			
			if ( n->child->brother->brother->brother->child )
				check_body(n->child->brother->brother->brother->child, t, global);
			
		}	

	}
	//senao
	else{
			
		if ( check_repeated_vars_in_func_def(n->child->brother->brother->child) )
			return -1;		
		//print_tree(n->child->brother->brother->child, 10);

		/*Node check_void = n->child->brother->brother->child;
		if ( strcmp(check_void->child->type, "Void") == 0 )
			if ( check_void->brother ){
				printf("Line %d, col %d: Invalid use of void type in declaration\n", check_void->child->numLinhas, 
																					check_void->child->numColunas - (int)strlen(check_void->child->type));
				return -1;
			}

		check_void = check_void->brother;

		while( check_void ){
			if ( strcmp(check_void->child->type, "Void") == 0 )
			{
				printf("Line %d, col %d: Invalid use of void type in declaration\n", check_void->child->numLinhas, 
																					check_void->child->numColunas - (int)strlen(check_void->child->type));
				return -1;
			}
			check_void = check_void->brother;
		}
		*/
		
		//pode adicionar a vontade
		
		elementoTabela *aux= global->first_element;
		while(aux->next!=NULL){
			aux=aux->next;
		}
		
		elementoTabela *dec= (elementoTabela*)malloc(sizeof(elementoTabela));
		
		dec->name=strdup(n->child->brother->value);
		dec->type= strdup(n->child->type);
		dec->type=(char*)realloc(dec->type,(128*6+1)*sizeof(char));
		dec->numParam = 1; 													
		strcat(dec->type,"(");
		
		Node paramtype=n->child->brother->brother->child;
		strcat(dec->type,paramtype->child->type);
		paramtype= paramtype->brother;
		
		while(paramtype){
			strcat(dec->type,",");
			strcat(dec->type,paramtype->child->type);
			paramtype= paramtype->brother;
			dec->numParam ++;
		}
		strcat(dec->type,")");
		
		for(int i = 0; dec->type[i]; i++){
  			dec->type[i] = tolower(dec->type[i]);
		}
		
		
		/*  adicionei */
		tabela *t = addTable(global, n, NULL);
		
		if ( init_table(t, n) == -1)
		{
			//removeElementoTabela(existe, global);
			//return -1;
		}

		dec->next=NULL;
		
		aux->next=dec;
		
		dec->definido = 1;
		t->definido = 1;
	
		if ( n->child->brother->brother->brother->child )
			check_body(n->child->brother->brother->brother->child, t, global);
			//;

		
	}
	return 0;
}

void removeElementoTabela(elementoTabela *e, tabela *g){
	tabela *aux = g;
	while( aux->next != NULL){
		aux = aux->next;
	}

	free(aux->next);
	aux->next = NULL;
}

tabela *getTabela(tabela *g,char *name){
	tabela *aux = g->next;

	while ( aux!=NULL ){
		if ( strcmp(aux->name, name) == 0)
			return aux;
		aux = aux->next;
	}
	return aux;

}
elementoTabela* getNextFunction(elementoTabela *e){
	elementoTabela *aux = e->next;
	while ( aux!=NULL ){
		if( strchr(aux->type, '(') !=NULL )
			return aux;
		aux = aux->next;
	}
	return aux;
}

elementoTabela* find_node(Node n, tabela *tab){
	if ( tab )
	{
		elementoTabela *aux= tab->first_element;
		while(aux!=NULL){
			if (strcmp(aux->name, n->value)==0 ){
				break;
			}
			aux=aux->next;
		}
		return aux;
	}
	else
		return NULL;
}



void check_program(Node root){
	tabela *g = init_symbol_table();

	Node current=root->child;
	
	while(current !=NULL){
		if(strcmp(current->type,"FuncDeclaration")==0){
			//func declarator if (current node type == func declarator)
			check_declarator(current, g);
		}
		else if(strcmp(current->type,"Declaration")==0){
			//declaration if (current node type == declaration)
			check_declaration(current, NULL, g);
		}
		else if(strcmp(current->type,"FuncDefinition")==0){
			//func definition if (current node type == func definition)
			check_definition(current, g);
			
			/*check_function(current->child->brother->brother);*/
		}	
		else{
			//erro
		}
		current=current->brother;
	}
	
	
	printTables(g);

	/*erros*/
	
	

	freeTables(g);
	return;
}

void freeTables(tabela *t){
	tabela *temp;
	while(t!=NULL){
		temp = t;
		if(t->name!=NULL)
			free(t->name);
		freeElements(t->first_element);
		
		t = t->next;
		free(temp);
	}
}

void freeElements(elementoTabela *e){
	elementoTabela *temp;
	while(e!=NULL){
		temp = e;
		if(e->name!=NULL)
			free(e->name);
		if(e->type!=NULL)
			free(e->type);
		if(e->param!=NULL)
			free(e->param);

		e = e->next;
		free(temp);
	}
}

/* #########################		O QUE EU FIZ		#############################*/
int init_table(tabela *t, Node n){
	Node no;
	elementoTabela *last, *e;		// para paramList
	
	elementoTabela *_return = (elementoTabela *)malloc(sizeof(elementoTabela));
	_return->name = strdup("return");
	_return->type = strdup( n->child->type );
	_return->numParam = 0;

	for(int i = 0; i<strlen(_return->type); i++)
		_return->type[i] = tolower(_return->type[i]);

	_return->definido = 1;
	_return->next = NULL;

	t->first_element = _return;

	if (strcmp(n->child->brother->brother->type, "ParamList") == 0){		// paramlist da func
		no = n->child->brother->brother->child;
		last = _return;

		while(no!=NULL ){
			if ( strcmp( no->child->type, "Void") != 0 && no->child->brother ) {
				if ( find_node(no->child->brother, t) != NULL ){
					//printf("Line %d, col %d: Symbol %s already defined\n", 	no->child->brother->numLinhas, 
					//														no->child->brother->numColunas - (int)strlen(no->child->brother->value), 
					//														e->name);
					// MEM LEAK TODO
					//return -1;
				}
				else
				{
					e = (elementoTabela *)malloc(sizeof(elementoTabela));
					e->name = strdup( no->child->brother->value );
					e->type = strdup( no->child->type );
					e->type[0] = tolower(e->type[0]);
					e->param = strdup("param");
					e->numParam = 0;
					e->definido = 1;
					e->next = NULL;
					last->next = e;
					last = e;
				}
			}
			no = no->brother;
		}
	}


	return 0;
}


tabela* addTable(tabela *g, Node n, elementoTabela *existe){
	tabela *temp = g;
	
	if ( existe == NULL ){
		while(temp->next != NULL){
			temp = temp->next;
		}
	}
	else{
		while ( temp->next && ( strcmp(temp->next->name, existe->name ) !=0  ) ){
			temp=temp->next;

		}
	}
	tabela *aux = (tabela *)malloc(sizeof(tabela));
	aux->name = strdup( n->child->brother->value );
	aux->first_element = NULL;
	
	if(temp!=NULL)
	{
		aux->next = temp->next;
		temp->next = aux;
	}
	
	return aux;
}

int isTerminal(Node n) {
	return (strcmp(n->type, "IntLit" ) == 0 || 
			strcmp(n->type, "RealLit") == 0 ||
			strcmp(n->type, "ChrLit" ) == 0 );
			
}

int typeCompare(char *type1, char *type2){
	return ( strcmp(type1, type2) == 0 ||
		   ((strcmp(type1, "int"   ) == 0 && strcmp(type2, "IntLit" )) || (strcmp(type2, "int"   ) == 0 && strcmp(type1, "IntLit" ) == 0)) || 
		   ((strcmp(type1, "double") == 0 && strcmp(type2, "RealLit")) || (strcmp(type2, "double") == 0 && strcmp(type1, "RealLit") == 0)) ||  
		   ((strcmp(type1, "char"  ) == 0 && strcmp(type2, "ChrLit" )) || (strcmp(type2, "char"  ) == 0 && strcmp(type1, "ChrLit" ) == 0)) 
		);
}

char* check_type(Node n){										//TODO TERMINAR E VERIFICAR SE É VALIDO E ONDE PODE SER USADO???
	/*
	char OP int = int || int OP char = int    ???????
	double OP * = double || * OP double = double
	short OP * = short || * OP short = short	?????
	*/
	if ( n->child && n->child->brother ){
		if ( strcmp(n->child->annotation, n->child->brother->annotation) == 0 ){
			return strdup(n->child->annotation);
		}
		else if ( strcmp(n->child->annotation, "undef") == 0 		||
				  strcmp(n->child->brother->annotation, "undef") == 0 )
			return strdup("undef");
		else
		{
			// TODO CHECK
			if ( strcmp(n->child->annotation, "double") == 0 || 
				 strcmp(n->child->brother->annotation, "double") == 0)
				return strdup("double");

			else if ( strcmp(n->child->annotation, "int") == 0 || 
				 	  strcmp(n->child->brother->annotation, "int") == 0)
				return strdup("int");

			else if ( strcmp(n->child->annotation, "short") == 0 || 
				 	  strcmp(n->child->brother->annotation, "short") == 0)
				return strdup("short");

			/*else if ( (strcmp(n->child->annotation, "int") == 0 && strcmp(n->child->brother->annotation, "char") == 0) || 
					  (strcmp(n->child->annotation, "char") == 0 && strcmp(n->child->brother->annotation, "int") == 0)   )
				return strdup("int");
			*/
			else
				return strdup(" NECESSARIO CALCULAR O TIPO ");
		}	
	}
	else if ( n->child && n->child->brother==NULL)
		return strdup(n->child->annotation);
	else
		return strdup("??????UNDEF?????????");
}


void check_body(Node n, tabela *t, tabela *global){
	Node current = n;
	elementoTabela *e;
	char temp[100];

	while( current != NULL){
		if ( strcmp(current->type, "Declaration") == 0 )
		{
			//print_tree(current, 10);

			check_declaration(current, t, global);

			/*current->child->type[0] = tolower(current->child->type[0]);
			
			if ( current->child->brother->brother &&
				 current->child->brother->brother->annotation && 
				 strcmp(current->child->brother->brother->annotation, "double") == 0){
				if ( strcmp(current->child->type, "double" ) != 0)
				{
					printf("Line %d, col %d: Conflicting types (got %s, expected %s)\n", current->child->brother->numLinhas, current->child->brother->numColunas -(int)strlen(current->child->brother->value),
																					 current->child->brother->brother->annotation ,
																					 current->child->type);

				}

			}
			

			current->child->type[0] = toupper(current->child->type[0]);
			*/
		}
		else if ( 							 			// operators
				  strcmp(current->type,"Add"  ) == 0 ||
				  strcmp(current->type,"Sub"  ) == 0 || 
				  strcmp(current->type,"Mul"  ) == 0 || 
				  strcmp(current->type,"Div"  ) == 0 || 
				  strcmp(current->type,"Plus" ) == 0 ||				
				  strcmp(current->type,"Minus") == 0  ){			// TODO NOT COMPLETE

			if ( current->child ){
				check_body(current->child, t,global);
				addAnnotation(current, check_type(current)) ;			//TODO CHECK IF CORRECT
				//addAnnotation(current, current->child->annotation);
			}
			

		}
		else if ( strcmp(current->type,"Store") == 0 )
		{	
			if ( current->child ){
				check_body(current->child, t,global);
				//addAnnotation(current, check_type(current)) ;			//TODO CHECK IF CORRECT
				addAnnotation(current, current->child->annotation);
			}
			
			/*
			if ( current->child->brother &&
				current->child->brother->annotation &&
				strcmp( current->child->annotation, current->child->brother->annotation ) ){

				printf("Line %d, col %d: Conflicting types (got %s, expected %s)\n", current->child->brother->numLinhas, current->child->brother->numColunas,
																					 current->child->brother->annotation ,
																					 current->child->annotation);
			}
			*/
			//current->child->type[0] = toupper(current->child->type[0]);
		}
		else if ( strcmp(current->type, "Mod") == 0 ||
				  strcmp(current->type, "Eq" ) == 0 ||
				  strcmp(current->type, "Ne" ) == 0 ||  				// booleans
				  strcmp(current->type, "Le" ) == 0 ||
				  strcmp(current->type, "Ge" ) == 0 ||
				  strcmp(current->type, "Lt" ) == 0 ||
				  strcmp(current->type, "Gt" ) == 0 ||
				  strcmp(current->type, "Not") == 0  ){

			addAnnotation(current, "int");
			if ( current->child ){
				check_body(current->child, t,global);
			}

		}
		else if ( strcmp(current->type, "Or"        ) == 0  || 
				  strcmp(current->type, "And"       ) == 0  || 
				  strcmp(current->type, "BitWiseAnd") == 0  ||  
				  strcmp(current->type, "BitWiseOr" ) == 0  ||
				  strcmp(current->type, "BitWiseXor") == 0   ){
			
			addAnnotation(current, "int") ;									// TODO CHECK
			if ( current->child ){											// TODO CHECK
				check_body(current->child, t,global);

			}
		}

		else if ( strcmp(current->type, "Comma") == 0 ){
			if ( current->child ){
				check_body(current->child, t,global);						// TODO CHECK
				addAnnotation(current, current->child->brother->annotation);	
			}
		}
		
		else if ( strcmp(current->type, "StatList") == 0 ||
				  strcmp(current->type, "While"   ) == 0 ||
				  strcmp(current->type, "If"      ) == 0 ||
				  strcmp(current->type, "Else"    ) == 0 ||
				  strcmp(current->type, "StatList") == 0 ||
				  strcmp(current->type, "Return"  ) == 0  ){
			// ignora as condicoes
			check_body(current->child, t, global);
		}
		else if( strcmp(current->type, "Call") == 0  ){
			e = find_node(current->child, t);
			if ( !e )
				e = find_node(current->child, global);
			if ( e )
			{
				strcpy(temp, e->type);
				addAnnotation(current->child, e->type);
				addAnnotation(current, strtok(temp, "(") );
				if ( current->child->brother )
					check_body( current->child->brother, t, global);
			}
			else{
				// #unknown func
				addAnnotation(current, "undef");			// TODO CHECK; Assm o check_body de cima nao acontece
															// logo nao fica anotado so call é que fica anotado
				
				if ( current->child->brother )
					check_body( current->child->brother, t, global);
				
			}
		}
		else if ( isTerminal(current) ){
			addAnnotation(current, getType(current) );
		}
		else if ( strcmp(current->type, "Id") == 0 ){
			e = find_node(current, t);
			if ( !e )
				e = find_node(current, global);

			if ( e )
				addAnnotation(current, e->type);
			else{
				// #unknown var
				addAnnotation(current, "undef");			// TODO CHECK
			}


		}
		
		current = current -> brother;
	}
	
	return;
}

char* getType(Node n){
	if ( strcmp( n->type, "IntLit") == 0 ||
		 strcmp( n->type, "ChrLit") == 0  ){
		return strdup("int");
	}
	else if ( strcmp( n->type, "RealLit") == 0 )
		return strdup("double");
	else
		return strdup("");
}
