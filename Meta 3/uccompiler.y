%{
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "y.tab.h"
#include "ast_tree.h"
#include "tabela_simbolos.h"

int yylex(void);
void yyerror (const char *s);

extern int LEXICAL, SINTATICO, SEMANTICO, numLine, numCol;
extern char* yytext;
extern int yyleng;

int ARVORE, NOT_ERRO;
Node root, temp;

%}



%token <id> CHAR ELSE IF INT SHORT DOUBLE RETURN VOID BITWISEAND BITWISEOR BITWISEXOR AND ASSIGN MUL COMMA DIV EQ GE GT LBRACE LE LPAR LT MINUS MOD NE NOT OR PLUS RBRACE RPAR SEMI WHILE ID CHRLIT INTLIT REALLIT RESERVED


%left COMMA
%right ASSIGN
%left OR
%left AND
%left BITWISEOR
%left BITWISEXOR
%left BITWISEAND
%left EQ NE
%left LT LE GT GE
%left PLUS MINUS
%left MUL DIV MOD
%right NOT
%left LPAR
%nonassoc NOT_ELSE
%nonassoc ELSE
%start Program

%union{
	struct node* tree_node;
	struct l_t * id;
}

%type <tree_node> FunctionsAndDeclarations FunctionDefinition FunctionBody DeclarationsAndStatements FunctionDeclaration FunctionDeclarator ParameterList ParameterDeclaration Declaration Declarator Statement Expr TypeSpec X2 X3 X4 ExprGeral ExprCall

%%

Program: FunctionsAndDeclarations											{if (ARVORE && NOT_ERRO) add_child(root,$1); }

FunctionsAndDeclarations : FunctionsAndDeclarations FunctionDefinition 		{if (ARVORE && NOT_ERRO) {add_brother($1,$2); $$=$1;}}
	| FunctionsAndDeclarations FunctionDeclaration 							{if (ARVORE && NOT_ERRO) {add_brother($1,$2); $$=$1;}}
	| FunctionsAndDeclarations Declaration									{if (ARVORE && NOT_ERRO) {add_brother($1,$2); $$=$1;}}
	| FunctionDefinition  													{if (ARVORE && NOT_ERRO) $$=$1;}
	| FunctionDeclaration  													{if (ARVORE && NOT_ERRO) $$=$1;}
	| Declaration 															{if (ARVORE && NOT_ERRO) $$=$1;}
	| error SEMI															{if (ARVORE && NOT_ERRO) NOT_ERRO=0;}
	;

FunctionDefinition : TypeSpec FunctionDeclarator FunctionBody 		{if (ARVORE && NOT_ERRO){
																		$$=create_node("FuncDefinition","", numLine, numCol);
																		add_child($$,$1);
																		add_brother($1,$2);
																		if ($3) 
																			add_brother($2,$3);} }
	;

FunctionBody : LBRACE RBRACE										{if (ARVORE && NOT_ERRO){
																		$$=create_node("FuncBody","", numLine, numCol);
																	}}
	| LBRACE DeclarationsAndStatements RBRACE						{if (ARVORE && NOT_ERRO){
																		$$=create_node("FuncBody","", numLine, numCol);
																		add_child($$, $2);
																	}}


DeclarationsAndStatements : Statement DeclarationsAndStatements 	{if (ARVORE && NOT_ERRO){
																		if($1)
																		{
																			add_brother($1,$2);
																			$$=$1;
																		}
																		else
																			$$=$2;
																	}}
	| Declaration DeclarationsAndStatements 						{if (ARVORE && NOT_ERRO){
																		add_brother($1,$2);
																		$$=$1;
																	}}
																	
	| Declaration 													{if (ARVORE && NOT_ERRO) $$=$1;}
	| Statement 													{if (ARVORE && NOT_ERRO) $$=$1;}
	;

FunctionDeclaration : TypeSpec FunctionDeclarator SEMI 				{if (ARVORE && NOT_ERRO){
																		$$=create_node("FuncDeclaration", "", numLine, numCol);
																		add_child($$, $1);
																		add_brother($1, $2);
																	}}
	;

FunctionDeclarator : ID LPAR ParameterList RPAR 					{if (ARVORE && NOT_ERRO){
																		$$=create_node("Id",$1->string, $1->line, $1->col);
																		temp=create_node("ParamList","", numLine, numCol);
																		add_child(temp,$3);
																		add_brother($$,temp);
																	}}

ParameterList : ParameterDeclaration X2 							{if (ARVORE && NOT_ERRO){
																		add_brother($1,$2);
																		$$=$1;
																	}}
	| ParameterDeclaration											{if (ARVORE && NOT_ERRO) $$=$1;}
	; 

X2: COMMA ParameterDeclaration 										{if (ARVORE && NOT_ERRO) $$=$2;}
	| X2 COMMA ParameterDeclaration 								{if (ARVORE && NOT_ERRO){
																		add_brother($1,$3);
																		$$=$1;
																	}}
	;

ParameterDeclaration : TypeSpec 									{if (ARVORE && NOT_ERRO){
																		$$=create_node("ParamDeclaration","", numLine, numCol);
																		add_child($$,$1);
																	}}
	| TypeSpec ID 													{if (ARVORE && NOT_ERRO){
																		add_brother($1,create_node("Id",$2->string, $2->line, $2->col)); $$=create_node("ParamDeclaration","", numLine, numCol);
																		add_child($$,$1);
																	}}
	; 
	

Declaration: TypeSpec X3 SEMI 										{if (ARVORE && NOT_ERRO){
																		$$=$2;
																		temp=$2;
																		while(temp){
																			strcpy(temp->child->type,$1->type);
																			temp->child->numLinhas = $1->numLinhas;
																			temp->child->numColunas = $1->numColunas;
																			temp=temp->brother;
																		}
																	}}
	;

X3:	X3 COMMA Declarator 											{if (ARVORE && NOT_ERRO){
																		add_brother($1,$3);
																		$$=$1;
																	}}
	| Declarator 													{if (ARVORE && NOT_ERRO) $$= $1;}
	;

TypeSpec : CHAR 													{if (ARVORE && NOT_ERRO) $$=create_node("Char","", numLine, numCol);}
	| INT 															{if (ARVORE && NOT_ERRO) $$=create_node("Int","", numLine, numCol);}
	| VOID 															{if (ARVORE && NOT_ERRO) $$=create_node("Void","", numLine, numCol);}
	| SHORT 														{if (ARVORE && NOT_ERRO) $$=create_node("Short","", numLine, numCol);}
	| DOUBLE 														{if (ARVORE && NOT_ERRO) $$=create_node("Double","", numLine, numCol);}
	;

Declarator : 	ID 													{if (ARVORE && NOT_ERRO) {
																		$$=create_node("Declaration", "", numLine, numCol);
																		add_child($$,create_node("","", numLine, numCol));
																		add_child($$,create_node("Id", $1->string, $1->line, $1->col));
																		}}
	| 	ID ASSIGN ExprGeral 										{if (ARVORE && NOT_ERRO){
																		$$=create_node("Declaration", "", numLine, numCol);
																		add_child($$,create_node("","", numLine, numCol));
																		add_child($$,create_node("Id", $1->string, $1->line, $1->col));
																		add_child($$,$3);
																	}}
	;

Statement : SEMI 													{if (ARVORE && NOT_ERRO) $$=NULL;}
	|	ExprGeral SEMI 													{if (ARVORE && NOT_ERRO) $$=$1;}
	|	LBRACE RBRACE 												{if (ARVORE && NOT_ERRO) $$=NULL;}
	|	LBRACE X4 RBRACE 											{if (ARVORE && NOT_ERRO) {
																		if($2){	
																			if($2->brother)
																			{
																				$$ = create_node("StatList", "", numLine, numCol);
																				add_child($$, $2);
																			}
																			else{
																				$$ = $2;
																			}
																		}
																		else{
																			$$=NULL;
																		}	
																	}}


	|	IF LPAR ExprGeral RPAR Statement %prec NOT_ELSE					{if (ARVORE && NOT_ERRO){
																		$$=create_node("If","", numLine, numCol);
																		add_child($$, $3);
																		if($5)
																		{	
																			temp = $5;
																		}
																		else{
																			temp = create_node("Null", "", numLine, numCol);
																		}
																		add_brother($3, temp);
																		add_brother($3, create_node("Null", "", numLine, numCol));

																	}}
	|	IF LPAR ExprGeral RPAR Statement ELSE Statement 			{if (ARVORE && NOT_ERRO){
																		$$=create_node("If","", numLine, numCol);
																		add_child($$, $3);
																		if($5)
																		{	
																			temp = $5;
																		}
																		else{
																			temp = create_node("Null", "", numLine, numCol);
																		}
																		add_brother($3, temp);
																		if($7)
																		{	
																			temp = $7;
																		}
																		else{
																			temp = create_node("Null", "", numLine, numCol);
																		}
																		add_brother($3, temp);
																	}}
	|	WHILE LPAR ExprGeral RPAR Statement 						{if (ARVORE && NOT_ERRO){
																		$$=create_node("While","", numLine, numCol);
																		add_child($$, $3);
																		if($5)
																		{	
																			temp = $5;
																		}
																		else{
																			temp = create_node("Null", "", numLine, numCol);
																		}
																		add_brother($3, temp);
																		

																	}}
	|	RETURN SEMI 												{if (ARVORE && NOT_ERRO){
																		$$=create_node("Return","", numLine, numCol);
																		add_child($$, create_node("Null", "", numLine, numCol));
																	}}
	|	RETURN ExprGeral SEMI 											{if (ARVORE && NOT_ERRO){
																		$$=create_node("Return","", numLine, numCol);
																		add_child($$, $2);
																	}}	
	|	LBRACE error RBRACE 										{if (ARVORE && NOT_ERRO) NOT_ERRO=0;}
	| 	error SEMI													{if (ARVORE && NOT_ERRO) NOT_ERRO=0;}
	;

X4: Statement 														{if (ARVORE && NOT_ERRO){ 
																		if($1)
																			$$=$1;
																		else
																			$$=NULL;
																	}}
	| X4 Statement 													{if (ARVORE && NOT_ERRO){
																		if($1 && $2)
																		{
																			add_brother($1, $2);
																			$$=$1;
																		}
																		else if(!$1 && $2)
																			$$=$2;
																		else if($1 && !$2)
																			$$=$1;
																		else
																			$$=$1;
																	}}
	;


ExprGeral: ExprGeral COMMA Expr 				{if (ARVORE && NOT_ERRO){
													$$=create_node("Comma", "", $2->line, $2->col);
													add_child($$, $1);
													add_brother($1, $3);
												}}
	| Expr 										{if (ARVORE && NOT_ERRO) $$=$1;}
	;

ExprCall: ExprCall COMMA Expr 					{if (ARVORE && NOT_ERRO){
													add_brother($1,$3);
													$$=$1;
												}}
	| Expr 										{if (ARVORE && NOT_ERRO) $$=$1;}
	;


Expr : 	Expr ASSIGN Expr						{if (ARVORE && NOT_ERRO){
													$$=create_node("Store", "", $2->line, $2->col);
													add_child($$, $1);
													add_brother($1, $3);
												}}					
	|	Expr PLUS Expr							{if (ARVORE && NOT_ERRO){
													$$=create_node("Add", "", $2->line, $2->col);
													add_child($$, $1);
													add_brother($1, $3);
												}}
	| 	Expr MINUS Expr							{if (ARVORE && NOT_ERRO){
													$$=create_node("Sub", "",  $2->line, $2->col);
													add_child($$, $1);
													add_brother($1, $3);
												}}
	| 	Expr MUL Expr 							{if (ARVORE && NOT_ERRO){
													$$=create_node("Mul", "",  $2->line, $2->col);
													add_child($$, $1);
													add_brother($1, $3);
												}}
	|	Expr DIV Expr 							{if (ARVORE && NOT_ERRO){
													$$=create_node("Div", "",  $2->line, $2->col);
													add_child($$, $1);
													add_brother($1, $3);
												}}
	|   Expr MOD Expr 							{if (ARVORE && NOT_ERRO){
													$$=create_node("Mod", "",  $2->line, $2->col);
													add_child($$, $1);
													add_brother($1, $3);
												}}
	|	Expr OR Expr 							{if (ARVORE && NOT_ERRO){
													$$=create_node("Or", "", $2->line, $2->col);
													add_child($$, $1);
													add_brother($1, $3);
												}}
	|	Expr AND Expr 							{if (ARVORE && NOT_ERRO){
													$$=create_node("And", "",  $2->line, $2->col);
													add_child($$, $1);
													add_brother($1, $3);
												}}
	| 	Expr BITWISEAND Expr 					{if (ARVORE && NOT_ERRO){
													$$=create_node("BitWiseAnd", "",  $2->line, $2->col);
													add_child($$, $1);
													add_brother($1, $3);
												}}
	|	Expr BITWISEOR Expr 					{if (ARVORE && NOT_ERRO){
													$$=create_node("BitWiseOr", "",  $2->line, $2->col);
													add_child($$, $1);
													add_brother($1, $3);
												}}
	|	Expr BITWISEXOR Expr 					{if (ARVORE && NOT_ERRO){
													$$=create_node("BitWiseXor", "",  $2->line, $2->col);
													add_child($$, $1);
													add_brother($1, $3);
												}}
	|	Expr EQ Expr 							{if (ARVORE && NOT_ERRO){
													$$=create_node("Eq", "",  $2->line, $2->col);
													add_child($$, $1);
													add_brother($1, $3);
												}}
	| 	Expr NE Expr 							{if (ARVORE && NOT_ERRO){
													$$=create_node("Ne", "",  $2->line, $2->col);
													add_child($$, $1);
													add_brother($1, $3);
												}}
	| 	Expr LE Expr 							{if (ARVORE && NOT_ERRO){
													$$=create_node("Le", "",  $2->line, $2->col);
													add_child($$, $1);
													add_brother($1, $3);
												}}
	|	Expr GE Expr 							{if (ARVORE && NOT_ERRO){
													$$=create_node("Ge", "",  $2->line, $2->col);
													add_child($$, $1);
													add_brother($1, $3);
												}}
	| 	Expr LT Expr 							{if (ARVORE && NOT_ERRO){
													$$=create_node("Lt", "",  $2->line, $2->col);
													add_child($$, $1);
													add_brother($1, $3);
												}}
	|	Expr GT Expr 							{if (ARVORE && NOT_ERRO){
													$$=create_node("Gt", "",  $2->line, $2->col);
													add_child($$, $1);
													add_brother($1, $3);
												}}
	|	PLUS Expr %prec NOT 					{if (ARVORE && NOT_ERRO){
													$$=create_node("Plus", "",  $1->line, $1->col);
													add_child($$, $2);
												}}
	|	MINUS Expr %prec NOT					{if (ARVORE && NOT_ERRO){
													$$=create_node("Minus", "",  $1->line, $1->col);
													add_child($$, $2);
												}}
	| 	NOT Expr 			 					{if (ARVORE && NOT_ERRO){
													$$=create_node("Not", "",  $1->line, $1->col);
													add_child($$, $2);
												}}
	|	ID LPAR RPAR 							{if (ARVORE && NOT_ERRO){
													$$=create_node("Call", "",  $1->line, $1->col);
													temp=create_node("Id", $1->string,  $1->line, $1->col);
													add_child($$, temp);
												}}
	|	ID LPAR ExprCall RPAR 						{if (ARVORE && NOT_ERRO){
													$$=create_node("Call", "", $1->line, $1->col);
													temp=create_node("Id", $1->string, $1->line, $1->col);
													add_child($$, temp);
													add_brother(temp, $3);
												}}
	|	ID 										{if (ARVORE && NOT_ERRO) $$=create_node("Id",$1->string, $1->line, $1->col);}
	|	INTLIT 									{if (ARVORE && NOT_ERRO) $$=create_node("IntLit",$1->string, $1->line, $1->col);}
	|	CHRLIT 									{if (ARVORE && NOT_ERRO) $$=create_node("ChrLit",$1->string, $1->line, $1->col);}
	|	REALLIT 								{if (ARVORE && NOT_ERRO) $$=create_node("RealLit",$1->string, $1->line, $1->col);}
	|	LPAR ExprGeral RPAR 					{if (ARVORE && NOT_ERRO) $$=$2;}
	| 	LPAR error RPAR 						{if (ARVORE && NOT_ERRO) NOT_ERRO=0;}
	|	ID LPAR error RPAR						{if (ARVORE && NOT_ERRO) NOT_ERRO=0;}
	;

%%

void yyerror(const char *s){
    printf("Line %d, col %d: %s: %s\n", numLine, numCol-yyleng, s, yytext);
    NOT_ERRO=0;
}

int main(int argc, char** argv){
    if(argc != 1)
    {
		if(strcmp(argv[1], "-l") == 0)
		{
			LEXICAL = 1;
        	SINTATICO = 0;
        	ARVORE = 0;
        	yylex();
    	}
        else if(strcmp(argv[1], "-t") == 0)
        {
            LEXICAL= 0;
            SINTATICO = 1;
            ARVORE = 1;
            NOT_ERRO = 1;
            root = create_node("Program","", 0, 0);
            yyparse();
            
            if(NOT_ERRO)
            {
                if(root != NULL)
                {
                    print_tree(root, 1);
                }
            }
			
            if(root != NULL)
            {
                free_tree(root);
            }
        }
        else if(strcmp(argv[1], "-2") == 0)
        {
        	LEXICAL= 0;
            SINTATICO = 1;
            ARVORE = 0;
            NOT_ERRO = 1;
            yyparse();
        }
        else if(strcmp(argv[1], "-s") == 0){
        	LEXICAL = 0;
        	SINTATICO = 1;
        	ARVORE = 1;
        	SEMANTICO = 1;
        	NOT_ERRO = 1;

        	root = create_node("Program","", 0,0);
            yyparse();
                        
            if(NOT_ERRO)
            {

                check_program(root);
                
                if(root != NULL)
                {
                    print_tree(root, 1);
                }

            }

            if(root != NULL)
            {
                free_tree(root);
            }

        }
    }
    else
    {
    	LEXICAL= 0;
        SINTATICO = 1;
        ARVORE = 0;
        yyparse();
    }
  	return 0;
}

