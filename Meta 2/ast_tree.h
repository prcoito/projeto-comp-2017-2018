typedef struct node* Node;

typedef struct node{
    Node brother;
    Node child;
    char* type;
    char* value;
} node;

Node create_node(char* nodeType, char* value);
void add_child(Node parent, Node child);
void add_brother(Node current, Node new_bro);
void free_tree(Node current);
void print_tree(Node current, int depth);
