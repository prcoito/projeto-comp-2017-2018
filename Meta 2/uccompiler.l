%{
	
	/*
	GRUPO: NemMaLembro

	Nome1: João Pedro Martins

	Numero1: 2015237605

	Email1: jpdmartins@student.dei.uc.pt

	Nome2: Paulo Coito

	Numero2: 2015239805

	Email2: prcoito@student.dei.uc.pt
	*/

	#include "y.tab.h"

	int LEXICAL = 0, SINTATICO;
	int numLine = 1;
	int numCol = 1;

	int inicioComentario[2];

%}

CHAR		char
ELSE		else
IF			if
INT			int
SHORT		short
DOUBLE		double
RETURN		return
VOID		void
BITWISEAND	"&"
BITWISEOR	"|"
BITWISEXOR	"^"
AND			"&&"
ASSIGN		"="
MUL			"*"
COMMA		","
DIV			"/"
EQ			"=="
GE			">="
GT			">"
LBRACE		"{"
LE			"<="
LPAR		"("
LT			"<"
MINUS		"-"
MOD			"%"
NE			"!="
NOT			"!"
OR			"||"
PLUS		"+"
RBRACE		"}"
RPAR		")"
SEMI		";"
WHILE		while

RESERVED	"["|"]"|"++"|"--"|"auto"|"break"|"case"|"const"|"continue"|"default"|"do"|"enum"|"extern"|"float"|"for"|"goto"|"inline"|"long"|"register"|"restrict"|"signed"|"sizeof"|"static"|"struct"|"switch"|"typedef"|"union"|"unsigned"|"volatile"|"_Bool"|"_Complex"|"_Imaginary"

INTLIT		[0-9]+

ID			([a-zA-Z]|"_")([a-zA-Z]+|[0-9]+|"_")*

CHRLIT          	'([^\n'\\]|("\\n"|"\\t"|"\\\\"|"\\'"|"\\\""|("\\"[0-7]{1,3})))'
UNTERMINATEDCHAR    '([^\n'\\]|\\.)*(\\)?
INVALIDCHAR 		'([^\n'\\]|\\.)*'

REALLIT		([0-9]+"."([0-9]+)?(("e"|"E")("+"|"-")?[0-9]+)?)|("."([0-9]+)(("e"|"E")("+"|"-")?[0-9]+)?)|([0-9]+("e"|"E")("+"|"-")?[0-9]+)

COMMENTINIT		"/*"	
COMMENTEND		"*/"
COMMENTLINE		"//"

%X	COMMENT COMMENTL


%%

{RESERVED}			{ if(LEXICAL) printf("RESERVED(%s)\n", yytext);	numCol+=yyleng; yylval.id=(char*)strdup(yytext); if(SINTATICO) return RESERVED;}
{CHAR}				{ if(LEXICAL) printf("CHAR\n"); 					numCol+=yyleng; if(SINTATICO) return CHAR;}
{ELSE}				{ if(LEXICAL) printf("ELSE\n"); 					numCol+=yyleng; if(SINTATICO) return ELSE;}
{IF}				{ if(LEXICAL) printf("IF\n");						numCol+=yyleng; if(SINTATICO) return IF;}
{INT}				{ if(LEXICAL) printf("INT\n"); 					numCol+=yyleng; if(SINTATICO) return INT;}
{SHORT}				{ if(LEXICAL) printf("SHORT\n");				 	numCol+=yyleng; if(SINTATICO) return SHORT;}
{DOUBLE}			{ if(LEXICAL) printf("DOUBLE\n"); 				numCol+=yyleng; if(SINTATICO) return DOUBLE;}
{RETURN}			{ if(LEXICAL) printf("RETURN\n");					numCol+=yyleng; if(SINTATICO) return RETURN;}
{VOID}				{ if(LEXICAL) printf("VOID\n"); 					numCol+=yyleng; if(SINTATICO) return VOID;}
{BITWISEAND}		{ if(LEXICAL) printf("BITWISEAND\n");			 	numCol+=yyleng; if(SINTATICO) return BITWISEAND;}
{BITWISEOR}			{ if(LEXICAL) printf("BITWISEOR\n"); 				numCol+=yyleng; if(SINTATICO) return BITWISEOR;}
{BITWISEXOR}		{ if(LEXICAL) printf("BITWISEXOR\n"); 			numCol+=yyleng; if(SINTATICO) return BITWISEXOR;}
{AND}				{ if(LEXICAL) printf("AND\n"); 					numCol+=yyleng; if(SINTATICO) return AND;}
{ASSIGN}			{ if(LEXICAL) printf("ASSIGN\n");		 			numCol+=yyleng; if(SINTATICO) return ASSIGN;}
{MUL}				{ if(LEXICAL) printf("MUL\n"); 					numCol+=yyleng; if(SINTATICO) return MUL;}
{COMMA}				{ if(LEXICAL) printf("COMMA\n"); 					numCol+=yyleng; if(SINTATICO) return COMMA;}
{DIV}				{ if(LEXICAL) printf("DIV\n");	 				numCol+=yyleng; if(SINTATICO) return DIV;}
{EQ}				{ if(LEXICAL) printf("EQ\n");						numCol+=yyleng; if(SINTATICO) return EQ;}
{GE}				{ if(LEXICAL) printf("GE\n"); 					numCol+=yyleng; if(SINTATICO) return GE;}
{GT}				{ if(LEXICAL) printf("GT\n"); 					numCol+=yyleng; if(SINTATICO) return GT;}
{LBRACE}			{ if(LEXICAL) printf("LBRACE\n"); 				numCol+=yyleng; if(SINTATICO) return LBRACE;}
{LE}				{ if(LEXICAL) printf("LE\n");			 			numCol+=yyleng; if(SINTATICO) return LE;}
{LPAR}				{ if(LEXICAL) printf("LPAR\n"); 					numCol+=yyleng; if(SINTATICO) return LPAR;}
{LT}				{ if(LEXICAL) printf("LT\n"); 					numCol+=yyleng; if(SINTATICO) return LT;}
{MINUS}				{ if(LEXICAL) printf("MINUS\n"); 					numCol+=yyleng; if(SINTATICO) return MINUS;}
{MOD}				{ if(LEXICAL) printf("MOD\n"); 					numCol+=yyleng; if(SINTATICO) return MOD;}
{NE}				{ if(LEXICAL) printf("NE\n"); 					numCol+=yyleng; if(SINTATICO) return NE;}
{NOT}				{ if(LEXICAL) printf("NOT\n"); 					numCol+=yyleng; if(SINTATICO) return NOT;}
{OR}				{ if(LEXICAL) printf("OR\n"); 					numCol+=yyleng; if(SINTATICO) return OR;}
{PLUS}				{ if(LEXICAL) printf("PLUS\n"); 					numCol+=yyleng; if(SINTATICO) return PLUS;}
{RBRACE}			{ if(LEXICAL) printf("RBRACE\n"); 				numCol+=yyleng; if(SINTATICO) return RBRACE;}
{RPAR}				{ if(LEXICAL) printf("RPAR\n"); 					numCol+=yyleng; if(SINTATICO) return RPAR;}
{SEMI}				{ if(LEXICAL) printf("SEMI\n"); 					numCol+=yyleng; if(SINTATICO) return SEMI;}
{WHILE}				{ if(LEXICAL) printf("WHILE\n"); 					numCol+=yyleng; if(SINTATICO) return WHILE;}

{ID} 				{ if(LEXICAL) printf("ID(%s)\n", yytext); 		numCol+=yyleng; yylval.id=(char*)strdup(yytext); if(SINTATICO) return ID;}
{INTLIT}			{ if(LEXICAL) printf("INTLIT(%s)\n", yytext); 	numCol+=yyleng; yylval.id=(char*)strdup(yytext); if(SINTATICO) return INTLIT;}	
{REALLIT}			{ if(LEXICAL) printf("REALLIT(%s)\n", yytext); 	numCol+=yyleng; yylval.id=(char*)strdup(yytext); if(SINTATICO) return REALLIT;}
{CHRLIT}			{ if(LEXICAL) printf("CHRLIT(%s)\n", yytext); 	numCol+=yyleng; yylval.id=(char*)strdup(yytext); if(SINTATICO) return CHRLIT;}


{UNTERMINATEDCHAR}	{ printf("Line %d, col %d: unterminated char constant\n", numLine, numCol); numCol+=yyleng; }
{INVALIDCHAR}		{ printf("Line %d, col %d: invalid char constant (%s)\n", numLine, numCol, yytext); numCol+=yyleng; }


{COMMENTINIT}			{ BEGIN COMMENT;inicioComentario[0]=numLine;inicioComentario[1]=numCol; numCol+=yyleng;}
<COMMENT>{COMMENTEND}	{ BEGIN 0; numCol+=yyleng; }
<COMMENT>.				{ numCol+=yyleng;}
<COMMENT>\n 			{ numLine++; numCol=1;}
<COMMENT><<EOF>>		{ printf("Line %d, col %d: unterminated comment\n", inicioComentario[0], inicioComentario[1]); BEGIN 0;}

{COMMENTLINE}		{ BEGIN COMMENTL; numCol+=yyleng; }
<COMMENTL>"\\"		{ numCol+=yyleng;  }
<COMMENTL>.			{ numCol+=yyleng; }
<COMMENTL>\n		{ BEGIN 0; numLine++; numCol=1; }		

" "|\t|\f 			{ numCol+=yyleng; }
[\r\n]|\r\n			{ numLine++; numCol=1; }
<<EOF>>				{ numCol++; return 0;}
.					{ printf("Line %d, col %d: illegal character (%s)\n",numLine, numCol,yytext); numCol+=yyleng; }


%%
int yywrap()
{
return 1;
}
