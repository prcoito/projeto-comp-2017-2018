%{
	
	/*
	GRUPO: NemMaLembro

	Nome1: João Pedro Martins

	Numero1: 2015237605

	Email1: jpdmartins@student.dei.uc.pt

	Nome2: Paulo Coito

	Numero2: 2015239805

	Email2: prcoito@student.dei.uc.pt
	*/


	int DEBUG = 0;
	int numLine = 1;
	int numCol = 1;

	int inicioChar[2];
	int inicioComentario[2];
%}

CHAR		char
ELSE		else
IF		if
INT		int
SHORT		short
DOUBLE		double
RETURN		return
VOID		void
BITWISEAND	"&"
BITWISEOR	"|"
BITWISEXOR	"^"
AND		"&&"
ASSIGN		"="
MUL		"*"
COMMA		","
DIV		"/"
EQ		"=="
GE		">="
GT		">"
LBRACE		"{"
LE		"<="
LPAR		"("
LT		"<"
MINUS		"-"
MOD		"%"
NE		"!="
NOT		"!"
OR		"||"
PLUS		"+"
RBRACE		"}"
RPAR		")"
SEMI		";"
WHILE		while

RESERVED	"["|"]"|"++"|"--"|"auto"|"break"|"case"|"const"|"continue"|"default"|"do"|"enum"|"extern"|"float"|"for"|"goto"|"inline"|"long"|"register"|"restrict"|"signed"|"sizeof"|"static"|"struct"|"switch"|"typedef"|"union"|"unsigned"|"volatile"|"_Bool"|"_Complex"|"_Imaginary"

INTLIT		[0-9]+

ID		([a-zA-Z]|"_")([a-zA-Z]+|[0-9]+|"_")*

CHRLIT          	'([^\n'\\]|("\\n"|"\\t"|"\\\\"|"\\'"|"\\\""|("\\"[0-7]{1,3})))'
UNTERMINATEDCHAR    '([^\n'\\]|\\.)*(\\)?
INVALIDCHAR 		'([^\n'\\]|\\.)*'

REALLIT		([0-9]+"."([0-9]+)?(("e"|"E")("+"|"-")?[0-9]+)?)|("."([0-9]+)(("e"|"E")("+"|"-")?[0-9]+)?)|([0-9]+("e"|"E")("+"|"-")?[0-9]+)

COMMENTINIT		"/*"	
COMMENTEND		"*/"
COMMENTLINE		"//"

%X	COMMENT COMMENTL


%%

{RESERVED}	{if(DEBUG) printf("RESERVED(%s)\n", yytext); numCol+=yyleng;}
{CHAR}		{if(DEBUG) printf("CHAR\n"); numCol+=yyleng;}
{ELSE}		{if(DEBUG) printf("ELSE\n"); numCol+=yyleng;}
{IF}		{if(DEBUG) printf("IF\n"); numCol+=yyleng;}
{INT}		{if(DEBUG) printf("INT\n"); numCol+=yyleng;}
{SHORT}		{if(DEBUG) printf("SHORT\n"); numCol+=yyleng;}
{DOUBLE}	{if(DEBUG) printf("DOUBLE\n"); numCol+=yyleng;}
{RETURN}	{if(DEBUG) printf("RETURN\n"); numCol+=yyleng;}
{VOID}		{if(DEBUG) printf("VOID\n"); numCol+=yyleng;}
{BITWISEAND}	{if(DEBUG) printf("BITWISEAND\n"); numCol+=yyleng;}
{BITWISEOR}	{if(DEBUG) printf("BITWISEOR\n"); numCol+=yyleng;}
{BITWISEXOR}	{if(DEBUG) printf("BITWISEXOR\n"); numCol+=yyleng;}
{AND}		{if(DEBUG) printf("AND\n"); numCol+=yyleng;}
{ASSIGN}	{if(DEBUG) printf("ASSIGN\n"); numCol+=yyleng;}
{MUL}		{if(DEBUG) printf("MUL\n"); numCol+=yyleng;}
{COMMA}		{if(DEBUG) printf("COMMA\n"); numCol+=yyleng;}
{DIV}		{if(DEBUG) printf("DIV\n"); numCol+=yyleng;}
{EQ}		{if(DEBUG) printf("EQ\n"); numCol+=yyleng;}
{GE}		{if(DEBUG) printf("GE\n"); numCol+=yyleng;}
{GT}		{if(DEBUG) printf("GT\n"); numCol+=yyleng;}
{LBRACE}	{if(DEBUG) printf("LBRACE\n"); numCol+=yyleng;}
{LE}		{if(DEBUG) printf("LE\n"); numCol+=yyleng;}
{LPAR}		{if(DEBUG) printf("LPAR\n"); numCol+=yyleng;}
{LT}		{if(DEBUG) printf("LT\n"); numCol+=yyleng;}
{MINUS}		{if(DEBUG) printf("MINUS\n"); numCol+=yyleng;}
{MOD}		{if(DEBUG) printf("MOD\n"); numCol+=yyleng;}
{NE}		{if(DEBUG) printf("NE\n"); numCol+=yyleng;}
{NOT}		{if(DEBUG) printf("NOT\n"); numCol+=yyleng;}
{OR}		{if(DEBUG) printf("OR\n"); numCol+=yyleng;}
{PLUS}		{if(DEBUG) printf("PLUS\n"); numCol+=yyleng;}
{RBRACE}	{if(DEBUG) printf("RBRACE\n"); numCol+=yyleng;}
{RPAR}		{if(DEBUG) printf("RPAR\n"); numCol+=yyleng;}
{SEMI}		{if(DEBUG) printf("SEMI\n"); numCol+=yyleng;}
{WHILE}		{if(DEBUG) printf("WHILE\n"); numCol+=yyleng;}
{ID} 		{if(DEBUG) printf("ID(%s)\n", yytext); numCol+=yyleng;}
{INTLIT}	{if(DEBUG) printf("INTLIT(%s)\n", yytext); numCol+=yyleng;}	
{REALLIT}	{if(DEBUG) printf("REALLIT(%s)\n", yytext); numCol+=yyleng;}
{CHRLIT}	{if(DEBUG) printf("CHRLIT(%s)\n", yytext); numCol+=yyleng;}
{UNTERMINATEDCHAR}		{printf("Line %d, col %d: unterminated char constant\n", numLine, numCol); numCol+=yyleng;}
{INVALIDCHAR}		{printf("Line %d, col %d: invalid char constant (%s)\n", numLine, numCol, yytext); numCol+=yyleng;}


{COMMENTINIT}			{BEGIN COMMENT;inicioComentario[0]=numLine;inicioComentario[1]=numCol; numCol+=yyleng;}
<COMMENT>{COMMENTEND}		{BEGIN 0; numCol+=yyleng;}
<COMMENT>.			{ numCol+=yyleng;}
<COMMENT>\n 			{numLine++; numCol=1;}

<COMMENT><<EOF>>		{printf("Line %d, col %d: unterminated comment\n", inicioComentario[0], inicioComentario[1]); return 0;}

{COMMENTLINE}			{BEGIN COMMENTL; numCol+=yyleng;}
<COMMENTL>"\\"			{ numCol+=yyleng;}
<COMMENTL>.			{ numCol+=yyleng;}
<COMMENTL>\n			{BEGIN 0; numLine++; numCol=1;}		

" "|\t|\f 			{numCol+=yyleng;}
[\r\n]|\r\n			{numLine++; numCol=1;}
.				{printf("Line %d, col %d: illegal character (%s)\n",numLine, numCol,yytext); numCol+=yyleng;}


%%

int main(int argc, char* argv[])
{
	if (argc>=2 && strcmp(argv[1], "-l")==0)
		DEBUG = 1;
	else{
		DEBUG = 0;
	}
	
	yylex();
	return 0;
}
int yywrap()
{
return 1;
}
