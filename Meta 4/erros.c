#include "ast_tree.h"
#include "tabela_simbolos.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

int check_void_var_declaration(Node n){
	
	if ( strcmp(n->type, "Void") == 0 )
	{
		printf("Line %d, col %d: Invalid use of void type in declaration\n", n->brother->numLinhas, 
																			 n->brother->numColunas - (int)strlen(n->brother->value) );
		return 1;
	}
	return 0;

}

int check_void_in_param_func_dec(Node check_void){

	if ( strcmp(check_void->child->type, "Void") == 0 )
		if ( check_void->brother ||  check_void->child->brother ){
			printf("Line %d, col %d: Invalid use of void type in declaration\n", check_void->child->numLinhas, 
																				check_void->child->numColunas - (int)strlen(check_void->child->type));
			return 1;
		}

	check_void = check_void->brother;

	while( check_void ){
		if ( strcmp(check_void->child->type, "Void") == 0 )
		{
			printf("Line %d, col %d: Invalid use of void type in declaration\n", check_void->child->numLinhas, 
																				check_void->child->numColunas - (int)strlen(check_void->child->type));
			return 1;
		}
		check_void = check_void->brother;
	}

	return 0;
}


char* createType(Node n){
	char *string_temp = (char *)malloc( (128*6+1)*sizeof(char) );

	strcpy(string_temp, n->child->type);
	strcat(string_temp,"(");
		

	Node paramtype=n->child->brother->brother->child;
	strcat(string_temp,paramtype->child->type);
	paramtype= paramtype->brother;
	
	while(paramtype){
		strcat(string_temp,",");
		strcat(string_temp,paramtype->child->type);
		paramtype= paramtype->brother;
		//dec->numParam ++;
	}
	strcat(string_temp,")");
	
	for(int i = 0; string_temp[i]; i++){
			string_temp[i] = tolower(string_temp[i]);
	}

	return string_temp;
}

int check_conflicting_type(Node n, elementoTabela *existe ){

	char* temp = createType(n);
	
	if( strcmp(existe->type, temp ) !=0){
		printf("Line %d, col %d: Conflicting types (got %s, expected %s)\n", n->child->brother->numLinhas  ,
																			 n->child->brother->numColunas - (int)strlen(n->child->brother->value),
																			 temp, existe->type);
		//while(1){;}
		return 1;
	}

	return 0;
}

int numParamNode(Node n){
	Node aux = n->child;
	int count = 0;
	while(aux!=NULL){
		if(strcmp(aux->type, "ParamDeclaration") == 0)
			count++;
		aux = aux->brother;
	}
	//printf("numParam %d\n", count);
	return count;
}

int check_repeated_vars_in_func_def(Node paramtype){
	//print_tree(paramtype, 40);


	int i,j;
	char *arrayTempChars[128];
	
	i = 0;
	while(paramtype){
		//print_tree(paramtype->child, 10);
		if ( paramtype->child && check_void_in_param_func_dec(paramtype) ){
			if ( paramtype->child->brother ){

			}
			return 1;
		}
			
		if ( paramtype->child->brother != NULL && paramtype->child->brother->value!=NULL )
		{
			arrayTempChars[i] = paramtype->child->brother->value;
			i++;
			break;
		}
		paramtype = paramtype->brother;			
	}

	if ( paramtype )
		paramtype = paramtype->brother;

	while(paramtype){

		j=0;
		while( j < i ){		// check if id in array;
			//printf("############################################################################################################\n");
			if ( paramtype->child && check_void_in_param_func_dec(paramtype) )
				return 1;
		
			if ( paramtype->child->brother !=NULL && paramtype->child->brother->value!=NULL ) 
			{
				//printf("%s %s %d\n", arrayTempChars[j], paramtype->child->brother->value , strcmp(arrayTempChars[j], paramtype->child->brother->value));
				//if ( check_void_var_declaration(paramtype->child) )
				//	return 1;

				//else
				if ( strcmp( arrayTempChars[j], paramtype->child->brother->value ) == 0 )
				{
					printf("Line %d, col %d: Symbol %s already defined\n",	paramtype->child->brother->numLinhas, 
																	  		paramtype->child->brother->numColunas - (int)strlen(arrayTempChars[j]),
																	  		arrayTempChars[j]);
					//return -1;
				}
				else
					arrayTempChars[i] = paramtype->child->brother->value;
				
			}
			j++;
		}
		if ( paramtype->child->brother !=NULL && paramtype->child->brother->value!=NULL )
			i++;			

		paramtype = paramtype->brother;
	}

	return 0;
}