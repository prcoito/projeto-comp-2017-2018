
#ifndef ast_tree

#define ast_tree

typedef struct node* Node;

typedef struct node{
    Node brother;
    Node child;
    int numLinhas;			// para analisador semantico
    int numColunas;			// para analisador semantico
    char* annotation;		// para analisador semantico
    char* type;
    char* value;

    int tempNum;				// para gerador de codigo. Guarda o resultad da var temp do resultado da operacao realizada
} node;

typedef struct l_t{
	char *string;
	int line, col;
} l_t;

#endif

l_t * getInfo(char* str);
Node create_node(char* nodeType, char* value, int numLinhas, int numColunas);
void add_child(Node parent, Node child);
void add_brother(Node current, Node new_bro);
void free_tree(Node current);
void print_tree(Node current, int depth);
