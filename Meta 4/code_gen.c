#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tabela_simbolos.h"
#include "ast_tree.h"
#include "erros.h"
#include "code_gen.h"
#include <ctype.h>

int tempNum = 1;

char *getParamsCall(Node n, int identation);
void cast(Node, char*, int);
char *getOperation(char *type, char *op);
int isBooleanOperator (char *type);

void ident(int identation);


char *getTypeLLVM(char* type){
	char *temp = strdup(type);
	//printf("\n[getTypeLLVM] %s_\n", type);
	if ( strcmp(type, "ChrLit") == 0 || strcmp(type, "char") == 0 || strcmp(type, "Char") == 0 )
		return strdup("i32");

	else if ( strcmp(type, "Short") == 0 || strcmp(type, "short") == 0)
		return strdup("i16");

	else if ( strcmp(type, "Int") == 0 || strcmp(type, "int") == 0 || strcmp(type, "IntLit") == 0 )
		return strdup("i32");

	else if ( strcmp(type, "Double") == 0 || strcmp(type, "RealLit") == 0 || strcmp(type, "double") == 0 )
		return strdup("double");
	else if ( strcmp(type, "Void") == 0 || strcmp(type, "void") == 0)
		return strdup("void");
	else
	{
		return getTypeLLVM(strtok(temp, "("));
	}
}

char *getAlign(char *type){
	//printf("[getAlign] %s\n", type);
	
	if ( strcmp(type, "ChrLit") == 0 || strcmp(type, "char") == 0  || strcmp(type, "Char") == 0)
		return strdup("4");

	else if ( strcmp(type, "Short") == 0 || strcmp(type, "short") == 0 )
		return strdup("2");

	else if ( strcmp(type, "Int") == 0 || strcmp(type, "int") == 0 || strcmp(type, "IntLit") == 0 )
		return strdup("4");

	else if ( strcmp(type, "Double") == 0 || strcmp(type, "RealLit") == 0 || strcmp(type, "double") == 0 )
		return strdup("8");

	else
	{
		return strdup("TODO");
	}
}

char *ascii(char *string){
    char value[10];
    char * pEnd;

    //printf("\t\tstring = %s\n", string);
    if (string[0] != '\''){
    	return string;
    }

	char *toReturn = (char *)malloc( (strlen(string)+1) * sizeof(char) );
	strncpy(toReturn, string + 1, strlen(string)-2 );
	
	//printf("\t\tTORETURN = %s\n", toReturn);
	if ( toReturn[0] == '\\' ){
		if ( strcmp(toReturn, "\\n") == 0 )
			sprintf(value, "%d", 10);
		else if ( strcmp(toReturn, "\\t")  == 0)
			sprintf(value, "%d", 9);
		else if ( strcmp(toReturn, "\\\\") == 0 )
			sprintf(value, "%d", 92);
		else if ( strcmp(toReturn, "\\'") == 0 )
			sprintf(value, "%d", 39);
		else if ( strcmp(toReturn, "\\\"") == 0 )
			sprintf(value, "%d", 34);
		else{
			// todas as sequencias de escape 
			//	\0, \00, \000, \1, .... 
			sprintf(value, "%ld", strtol( toReturn+1 , &pEnd, 8) );
		}
	}
	else{
		sprintf(value, "%d", toReturn[0]);
	}
	

	return strdup(value);

}

char *getInitValue(char *type, Node n){
	//printf("[getInitValue] %s value = %s\n", type, (n!=NULL)?n->value:"NO_VALUE");
	char temp[50];

	if ( strcmp(type, "ChrLit") == 0 || strcmp(type, "char") == 0  || strcmp(type, "Char") == 0)
		if ( n ){
			if ( n->tempNum != 0)
			{
				//sprintf(temp, "%%t%d", n->tempNum);
				sprintf(temp, "%%%d", n->tempNum);
				return strdup(temp);
			}
			return ascii( n->value ) ;
		}
		else
			return strdup("0");

	else if ( strcmp(type, "Short") == 0 || strcmp(type, "short") == 0 )
		if ( n ){
			if ( n->tempNum != 0)
			{
				//sprintf(temp, "%%t%d", n->tempNum);
				sprintf(temp, "%%%d", n->tempNum);
				return strdup(temp);
			}
			//return strdup(n->value);
			else
			{
				return strdup(n->value);
			}
		}
		else
			return strdup("0");

	else if ( strcmp(type, "Int") == 0 || strcmp(type, "int") == 0 || strcmp(type, "IntLit") == 0 )
		if ( n ){
			if ( n->tempNum != 0)
			{
				//sprintf(temp, "%%t%d", n->tempNum);
				sprintf(temp, "%%%d", n->tempNum);
				return strdup(temp);
			}
			else
			{
				if ( n->value[0] == '0')
				{
					//printf("atoi(%s) = %d, octal(%d) = %o\n", n->value, atoi(n->value), atoi(n->value), atoi(n->value));
					//sprintf(temp, "%o", n->value);
					int r;
					sscanf(n->value, "%o", &r);
					sprintf(temp, "%d", r);
					return strdup(temp);
				}
				else
					return strdup(n->value);
			}
		}
		else
			return strdup("0");

	else if ( strcmp(type, "Double") == 0 || strcmp(type, "RealLit") == 0 || strcmp(type, "double") == 0 )
		if ( n ){
			if ( n->tempNum != 0)
			{
				//sprintf(temp, "%%t%d", n->tempNum);
				sprintf(temp, "%%%d", n->tempNum);
				return strdup(temp);
			}
			if ( n->value[0] == '.' )
			{
				sprintf(temp, "0%s", n->value);
				return strdup(temp);
			}
			else{
				// inicia pos '.' -1
				// verificar se existe 'e':
				// se sim ver existem um ponto antes do 'e'
				double d;
				sscanf(n->value, "%lf", &d);

				sprintf(temp, "%e", d);
				return strdup(temp);
				//return strdup(n->value);
			}
		}
		else
			return strdup("0.0");

	else if ( strcmp(type, "Void") == 0 || strcmp(type, "void") == 0)
		return strdup("");

	else
	{
		return strdup("TODO");
	}

}


void getParams(tabela *t){
	int param  = 0;
	
	elementoTabela *e = t->first_element;
	while ( e != NULL && param==0){
		if(e->param){
			
			printf("%s %%arg.%s", getTypeLLVM(e->type), e->name );
			
			param++;
		}

		e = e->next;
	}

	while ( e != NULL){
		if(e->param){
			
			printf(", %s %%arg.%s", getTypeLLVM(e->type), e->name );
			
			param++;
		}

		e = e->next;
	}
	
	return;

}

void getParamsDeclaration(Node n){
	//print_tree(n, 10);

	Node param = n->child->child; 
	
	while ( param != NULL){
		
		printf("%s,", getTypeLLVM(param->child->type) );
			
		param = param->brother;
	}
	
	
	return;

}

void gen_all_params_in_function_declarations (Node n, int identation, tabela *t){
	//Node current = n;
	elementoTabela *e = t->first_element;
	char *type  = (char *)malloc(sizeof(char));
	char *align = (char *)malloc(sizeof(char));

	while( e != NULL){
		if ( e->param && strcmp(e->param, "param") == 0 ){
			free(type);
			free(align);

			type  = getTypeLLVM	(e->type);
			align = getAlign	(e->type);

			// %1 = alloca i32, align 4
  			// store i32 %a, i32* %1, align 4
  			ident(identation);
  			printf("%%%s = alloca %s, align %s\n", e->name, type, align);
			ident(identation);
			printf("store %s %%arg.%s, %s* %%%s, align %s\n", 	type,
																e->name,
																type,
																e->name,
																align);

		}

		e = e->next;
	}

}
void code_gen(Node n, int identation, tabela *t){
	Node current = n;
	tabela *tempTable;

	char *type  = (char *)malloc(sizeof(char));
	char *value = (char *)malloc(sizeof(char));
	char *align = (char *)malloc(sizeof(char));

	printf("declare i32 @putchar(i32)\n");
	printf("declare i32 @getchar()\n");
	printf("\n");

	tempTable = getTabela(t, "main");
	if ( tempTable == NULL){
		printf("define i32 @main(){\n\tret i32 0\n}\n\n");
	}


	while( current != NULL){
		
		if(strcmp(current->type,"FuncDeclaration")==0){
			/*
			
			//code_gen(current->child, identation, t);
			tempTable = getTabela(t, current->child->brother->value);
			
			if ( tempTable->definido == 0 )
			{
				printf("\ndeclare %s @%s(", getTypeLLVM(current->child->type), current->child->brother->value);

				getParamsDeclaration(current->child->brother);

				printf(")\n");
			}
			
			*/
			
		}
		
		else if(strcmp(current->type,"Declaration")==0){							// mas neste caso tenho duvida logo perguntar ao prof		

			// 		AINDA NAO DETETA EXPRESSOES So deteta o Plus e Minus, visto que nao sei como "declarar" as var temp
			/*if ( current->child->brother->brother )
				gen_body_code(current->child->brother->brother, identation, t);*/

			free(type);
			free(value);
			free(align);

			type  = getTypeLLVM	(current->child->type);
			align = getAlign	(current->child->type);
			//exemplo @c = global i32 10, align 4
			//exemplo @d = global double 9.000000e+00, align 8
			//exemplo @d = common global double 0.0, align 8
			printf("@%s = ", current->child->brother->value);
			if ( current->child->brother->brother == NULL )
				printf("common ");

			printf("global %s ", type);
			/* commentar este if ... else if ... else ... depois de saber a parte de cima */
			if ( current->child->brother->brother && strcmp(current->child->brother->brother->type, "Minus") == 0 )
			{
				value = getInitValue(current->child->type, current->child->brother->brother->child);
				printf("-%s", value);
			}
			else if ( current->child->brother->brother && strcmp(current->child->brother->brother->type, "Plus") == 0 )
			{
				value = getInitValue(current->child->type, current->child->brother->brother->child);
				printf("%s", value);
			}
			else
			{
				value = getInitValue(current->child->type, current->child->brother->brother);
				printf("%s", value);
			}

			/*
			// descomentar este depois de a parte de cima estiver comentada e a primeira parte estiver bem
			value = getInitValue(current->child->type, current->child->brother->brother);
			printf("%s", value);
			*/

			printf(", align %s\n", align);				
	
		}
		
		else if(strcmp(current->type,"FuncDefinition")==0){

			printf("\ndefine %s @%s(", getTypeLLVM(current->child->type), current->child->brother->value);
			tempTable = getTabela(t, current->child->brother->value);
			getParams(tempTable);

			printf("){\n");

			if ( current->child->brother->brother->brother->child ){
				
				/*se se poder fazer acesso direto, isto é, se nao levar arg. isto nao é necessario*/
				gen_all_params_in_function_declarations ( current->child->brother->brother->brother->child, identation+1, tempTable);

				tempNum = 1;
				gen_body_code(current->child->brother->brother->brother->child, identation+1, tempTable);
			}


			/*ret val*/

			ident(identation+1);
			printf("ret %s %s\n", getTypeLLVM(current->child->type), getInitValue(current->child->type, NULL) );
			printf("\n}\n");

		}	

		

		current = current -> brother;
	}
	
	free(type);
	return;
}

void gen_body_code(Node n, int identation, tabela *t ){
	Node current = n;
	int tempNumFilho1, labelInit,tempNumFilho2;

	char *type  	= (char *)malloc(sizeof(char));
	char *value 	= (char *)malloc(sizeof(char));
	char *align 	= (char *)malloc(sizeof(char));
	char *operation = (char *)malloc(sizeof(char));

	while( current != NULL){
		
		if(strcmp(current->type,"Declaration")==0){
			// faz-se primeiro o gen_body code para saber qual o valor a atribuir se nao for direto
			ident(identation);

			free(type);
			free(align);

			type  = getTypeLLVM	(current->child->type);
			align = getAlign	(current->child->type);
		
			/* exemplos
			 	%al = alloca i8, align 1
				%bl = alloca i16, align 2
				%cl = alloca i32, align 4
			 	%dl = alloca double, align 8
			  
			  	%oitol = alloca i32, align 4
			  	store i32 8, i32* %oitol, align 4
			  	
			  	%nove_point_zerol = alloca double, align 8
			  	store double 9.000000e+00, double* %nove_point_zerol, align 8

			*/

			printf("%%%s = alloca %s, align %s\n", current->child->brother->value, type, align);
			if ( current->child->brother->brother )
			{
				gen_body_code(current->child->brother->brother, identation, t);
				
				//atençao aos casts
				if ( strcmp(current->child->type, "Short") ==  0)
					cast(current->child->brother->brother, "short", identation);
				else
					cast(current->child->brother->brother, current->child->type, identation);

				ident(identation);

				printf("store %s %%%d, %s* %%%s, align %s\n", 	type,
																current->child->brother->brother->tempNum,
																type,
																current->child->brother->value,
																align);	
			
			
			}		
	

		}

		else if ( 							 			// operators
				  strcmp(current->type, "Add"  ) == 0 ||
				  strcmp(current->type, "Sub"  ) == 0 || 
				  strcmp(current->type, "Mul"  ) == 0 || 
				  strcmp(current->type, "Div"  ) == 0 ||
				  strcmp(current->type, "Mod"  ) == 0 ||
				  strcmp(current->type, "BitWiseAnd") == 0  ||  
				  strcmp(current->type, "BitWiseOr" ) == 0  ||
				  strcmp(current->type, "BitWiseXor") == 0  ){				/* se calhar será necessario adicionar nuw e nsw  */

			if ( current->child ){
				gen_body_code(current->child, identation, t);
				
			}

			Node toCast = current->child;
			while ( toCast ){
				cast( toCast, current->annotation, identation );
				toCast = toCast->brother;
			}

			ident(identation);

			free(operation);
			free(type);

			type  	  = getTypeLLVM	(current->annotation);
			operation = getOperation(current->annotation, current->type);

			printf("%%%d = %s %s %%%d, %%%d\n", tempNum, operation, type, current->child->tempNum, current->child->brother->tempNum);			

			current->tempNum = tempNum;
			tempNum++;

		}
		else if ( strcmp(current->type,"Plus" ) == 0 ||				
				  strcmp(current->type,"Minus") == 0  ){						// Plus e minus sao add

			if ( current->child ){
				gen_body_code(current->child, identation, t);
				
			}

			ident(identation);
			free(type);
			free(operation);
			free(value);

			operation = getOperation(current->annotation, current->type);
			type = getTypeLLVM(current->annotation);
			value = getInitValue(current->annotation, NULL);

			if ( current->child->tempNum != 0) 
				printf("%%%d = %s %s %s, %%%d \n", tempNum, operation, type, value, current->child->tempNum);
			else
				printf("%%%d = %s %s %s, %s \n", tempNum, operation, type, value, current->child->value);
			
			current->tempNum = tempNum;
			tempNum++;

		}
		else if ( strcmp(current->type,"Store") == 0 )
		{	
			if ( current->child->brother ){
				gen_body_code(current->child->brother, identation, t);
				
			}

			free(type);
			free(align);
			type = getTypeLLVM(current->child->annotation );
			align = getAlign(current->child->annotation);

			//ver casts
			cast(current->child->brother, current->child->annotation, identation);

			ident(identation);
			
			elementoTabela *e = find_node(current->child, t);
			if ( e == NULL )
			{
				printf("store %s %%%d, %s* @%s, align %s\n", 	type,
																current->child->brother->tempNum,
																type,
																current->child->value,
																align);
				
			}
			else
			{
				printf("store %s %%%d, %s* %%%s, align %s\n", 	type,
																current->child->brother->tempNum,
																type,
																current->child->value,
																align);
			}

			/*jpmartins_fix: store deve guardar o temp onde a variavel esta guardada*/
			/*current->tempNum = tempNum; esta errado*/
			current->tempNum = current->child->brother->tempNum;
			
		}
		else if ( strcmp(current->type, "Eq" ) == 0 ||
				  strcmp(current->type, "Ne" ) == 0 ||  				// booleans
				  strcmp(current->type, "Le" ) == 0 ||
				  strcmp(current->type, "Ge" ) == 0 ||
				  strcmp(current->type, "Lt" ) == 0 ||
				  strcmp(current->type, "Gt" ) == 0  ){

			if ( current->child ){
				gen_body_code(current->child, identation, t);
			}

			free(type);
			free(operation);
			
			Node toCast = current->child;
			if ( strcmp(toCast->annotation, "double") == 0 || strcmp(toCast->brother->annotation, "double") == 0 ||
				 strcmp(toCast->type, "Double") == 0 || strcmp(toCast->brother->type, "Double") == 0){
				while ( toCast ){
					cast( toCast, "double", identation );
					toCast = toCast->brother;
				}
				type = getTypeLLVM("double");
				operation = getOperation("double", current->type);
			}
			else
			{
				while ( toCast ){
					cast( toCast, current->child->annotation, identation );
					toCast = toCast->brother;
				}
				type = getTypeLLVM(current->child->annotation);
				operation = getOperation(current->child->annotation, current->type);
			}


			ident(identation);

			printf("%%%d = %s %s %%%d, %%%d\n", tempNum, operation, type, current->child->tempNum, current->child->brother->tempNum);
			tempNum++;
			
			ident(identation);
			printf("%%%d = zext i1 %%%d to i32\n", tempNum, tempNum-1);

			current->tempNum = tempNum;
			tempNum++;

		}
		else if ( strcmp(current->type, "Or") == 0  ){
			/*
				EXEMPLO OR:

				  %24 = load i32, i32* %a, align 4
				  %25 = icmp slt i32 %24, 1
				  br i1 %25, label %26, label %29

				; <label>:26                                      ; preds = %21
				  %27 = load i32, i32* %b, align 4
				  %28 = icmp sgt i32 %27, 3
				  br label %29

				; <label>:29                                      ; preds = %26, %21
				  %30 = phi i1 [ false, %21 ], [ %28, %26 ]
			*/
			
			Node backup = current->child->brother;
			current->child->brother = NULL;
			gen_body_code(current->child, identation, t);

			labelInit = tempNum;
			printf("br label %%InitOR%d\n", labelInit);
			printf("\nInitOR%d:\n", labelInit );
			
			free(type);
			free(value);
			free(operation);
			
			Node toCast = current->child;
			if ( strcmp(toCast->annotation, "double") == 0 ){
				cast( toCast, "double", identation );
				
				type = getTypeLLVM("double");
				operation = getOperation("double", "Ne" );
				value = getInitValue("double", NULL);
			}
			else
			{
				cast( toCast, "int", identation );
				
				type = getTypeLLVM("int");
				operation = getOperation("int", "Ne");
				value = getInitValue("int", NULL);
			}
			
			ident(identation);
			printf("%%%d = %s %s %%%d, %s\n", tempNum, operation, type, current->child->tempNum, value);
			
			tempNumFilho1 = tempNum;
			
			tempNum++;
			
			ident(identation);
			
			printf("br i1 %%%d, label %%EndOR%d, label %%FirstOfORIsFalse%d\n", tempNum-1, labelInit, labelInit);
			printf("\nFirstOfORIsFalse%d:\n", labelInit );
			
			current->child->brother = backup;
			gen_body_code(current->child->brother, identation, t);
			
			free(type);
			free(value);
			free(operation);
			
			toCast = current->child->brother;
			if ( strcmp(toCast->annotation, "double") == 0 ){
				cast( toCast, "double", identation );
				
				type = getTypeLLVM("double");
				operation = getOperation("double", "Ne");
				value = getInitValue("double", NULL);
			}
			else
			{
				cast( toCast, "int", identation );
				
				type = getTypeLLVM("int");
				operation = getOperation("int", "Ne");
				value = getInitValue("int", NULL);
			}
			
			ident(identation);
			printf("%%%d = %s %s %%%d, %s\n", tempNum, operation, type, toCast->tempNum, value);
			
			tempNumFilho2 = tempNum;
			tempNum++;
			

			ident(identation);
			printf("br label %%EndOR%d\n", labelInit);
			
			printf("\nEndOR%d:\n", labelInit );			
			
			ident(identation);
			printf("%%%d = phi i1 [ true, %%InitOR%d ], [ %%%d, %%FirstOfORIsFalse%d ]\n", tempNum, labelInit, tempNumFilho2, labelInit);
			
			tempNum++;
			
			ident(identation);
			printf("%%%d = zext i1 %%%d to i32\n", tempNum, tempNum-1);

			current->tempNum = tempNum;
			tempNum++;

		}
		else if (strcmp(current->type, "And") == 0   )
		{
			/*	EXEMPLO AND:

				  %16 = load i32, i32* %a, align 4
				  %17 = icmp slt i32 %16, 1
				  br i1 %17, label %18, label %21

				; <label>:18                                      ; preds = %0
				  %19 = load i32, i32* %b, align 4
				  %20 = icmp sgt i32 %19, 3
				  br label %21

				; <label>:21                                      ; preds = %18, %0
				  %22 = phi i1 [ false, %0 ], [ %20, %18 ]

			*/

			// if primeiro operador do AND for falso:
			//		salta para o fim
			// resultado do AND = false e veio do inicio senao vai ser igual a resultado do ultimo operador do and 
		

			Node backup = current->child->brother;
			current->child->brother = NULL;
			gen_body_code(current->child, identation, t);

			labelInit = tempNum;
			printf("br label %%InitAND%d\n", labelInit);
			printf("\nInitAND%d:\n", labelInit );

			free(type);
			free(value);
			free(operation);
			
			Node toCast = current->child;
			if ( strcmp(toCast->annotation, "double") == 0 ){
				cast( toCast, "double", identation );
				
				type = getTypeLLVM("double");
				operation = getOperation("double", "Ne");
				value = getInitValue("double", NULL);
			}
			else
			{
				cast( toCast, "int", identation );
				
				type = getTypeLLVM("int");
				operation = getOperation("int", "Ne");
				value = getInitValue("int", NULL);
			}

			ident(identation);
			printf("%%%d = %s %s %%%d, %s\n", tempNum, operation, type, toCast->tempNum, value);
			
			tempNumFilho1 = tempNum;
			
			tempNum++;
			
			ident(identation);
			printf("br i1 %%%d, label %%FirstOfANDIsTrue%d, label %%EndAnd%d\n", tempNum-1, labelInit, labelInit);
			printf("\nFirstOfANDIsTrue%d:\n", labelInit );
			
			current->child->brother = backup;
			gen_body_code(current->child->brother, identation, t);
			
			free(type);
			free(value);
			free(operation);
			
			toCast = current->child->brother;
			if ( strcmp(toCast->annotation, "double") == 0 ){
				cast( toCast, "double", identation );
				
				type = getTypeLLVM("double");
				operation = getOperation("double", "Ne");
				value = getInitValue("double", NULL);
			}
			else
			{
				cast( toCast, "int", identation );
				
				type = getTypeLLVM("int");
				operation = getOperation("int", "Ne");
				value = getInitValue("int", NULL);
			}

			
			ident(identation);
			printf("%%%d = %s %s %%%d, %s\n", tempNum, operation, type, toCast->tempNum, value);
			
			tempNumFilho2 = tempNum;
			tempNum++;
			

			ident(identation);
			printf("br label %%EndAnd%d\n", labelInit);
			printf("\nEndAnd%d:\n", labelInit);
			
			ident(identation);
			printf("%%%d = phi i1 [ false, %%InitAND%d ], [ %%%d, %%FirstOfANDIsTrue%d ]\n", tempNum, labelInit, tempNumFilho2, labelInit);
			tempNum++;
			
			ident(identation);
			printf("%%%d = zext i1 %%%d to i32\n", tempNum, tempNum-1);

			current->tempNum = tempNum;
			tempNum++;
		}

		else if ( strcmp(current->type, "Not") == 0  ){
			/*
			http://lists.llvm.org/pipermail/llvm-dev/2015-April/084376.html

			LLVM doesn't have a "logical neg" (or "not") operator. That's a C thing. Do
			a compare against 0 to create an i1 result, then zero extend the i1 to the
			size of integer result you want.

			*/
			
			if ( current->child ){
				gen_body_code(current->child, identation, t);
			}

			free(type);
			free(operation);

			operation = getOperation(current->child->annotation, "Eq");
			type = getTypeLLVM(current->child->annotation);

			ident(identation);
			printf("%%%d = %s %s %%%d, 0\n", tempNum, operation, type, current->child->tempNum);
			tempNum++;

			ident(identation);
			printf("%%%d = zext i1 %%%d to i32\n", tempNum, tempNum-1);

			current->tempNum = tempNum;
			tempNum++;

		}

		else if ( strcmp(current->type, "Comma") == 0 ){
			if ( current->child ){															/*TODO*/
				gen_body_code(current->child, identation, t);
				
			}

			Node n_atual = current->child;
			while ( n_atual ){
				current->tempNum = n_atual->tempNum;
				n_atual = n_atual->brother;
			}

		}
		else if ( strcmp(current->type, "If"      ) == 0 /*||
				  strcmp(current->type, "Else"    ) == 0 ||*/ ){

			//print_tree(current, 10);

			Node backup = current->child->brother;
			current->child->brother = NULL;
			gen_body_code(current->child, identation, t);
			// label init
			labelInit = tempNum;
			printf("\n");
			ident(identation);
			printf("br label %%IF_INIT%d\n", labelInit);
			printf("IF_INIT%d:\n", labelInit);

			Node toCast = current->child;
			if ( strcmp(toCast->annotation, "double") == 0 ){
				cast( toCast, "double", identation );
				
				type = getTypeLLVM("double");
				operation = getOperation("double", "Ne");
				value = getInitValue("double", NULL);
			}
			else
			{
				cast( toCast, "int", identation );
				
				type = getTypeLLVM("int");
				operation = getOperation("int", "Ne");
				value = getInitValue("int", NULL);
			}

			
			ident(identation);
			printf("%%%d = %s %s %%%d, %s\n", tempNum, operation, type, toCast->tempNum, value);
			ident(identation);
			printf("br i1 %%%d, label %%IF_TRUE%d, label %%IF_FALSE%d\n", tempNum, labelInit, labelInit);
			tempNum++;
			

			// label if true
			printf("IF_TRUE%d:\n", labelInit);
			current->child->brother = backup;
			backup = current->child->brother->brother; // else se existir;
			current->child->brother->brother = NULL;
			gen_body_code(current->child->brother, identation, t);
			ident(identation);
			printf("br label %%IF_END%d\n", labelInit);
		

			// label if false
			printf("IF_FALSE%d:\n", labelInit);			
			current->child->brother->brother = backup;
			gen_body_code(current->child->brother->brother, identation, t);
			ident(identation);
			printf("br label %%IF_END%d\n", labelInit);

			//label if end
			printf("IF_END%d:\n", labelInit);	
		
		}
		else if ( strcmp(current->type, "While"   ) == 0 ){
			printf("\n");
			labelInit = tempNum;

			ident(identation);
			printf("br label %%WHILE_INIT%d\n", labelInit);
			printf("WHILE_INIT%d:\n", labelInit);

			Node backup = current->child->brother;
			current->child->brother = NULL;
			
			gen_body_code(current->child, identation, t);
			
			Node toCast = current->child;
			if ( strcmp(toCast->annotation, "double") == 0 ){
				cast( toCast, "double", identation );
				
				type = getTypeLLVM("double");
				operation = getOperation("double", "Ne");
				value = getInitValue("double", NULL);
			}
			else
			{
				cast( toCast, "int", identation );
				
				type = getTypeLLVM("int");
				operation = getOperation("int", "Ne");
				value = getInitValue("int", NULL);
			}

			
			
			ident(identation);
			printf("%%%d = %s %s %%%d, %s\n", tempNum, operation, type, toCast->tempNum, value);
			ident(identation);
			printf("br i1 %%%d, label %%WHILE_TRUE%d, label %%WHILE_END%d\n", tempNum, labelInit, labelInit);
			tempNum++;

			current->child->brother = backup;

			printf("WHILE_TRUE%d:\n", labelInit);
			gen_body_code(current->child->brother, identation, t);
			printf("br label %%WHILE_INIT%d\n", labelInit);
			

			printf("WHILE_END%d:\n", labelInit);
			


		}
		else if ( strcmp(current->type, "StatList") == 0 ) {
			

			gen_body_code(current->child, identation, t);

		}
		else if( strcmp(current->type, "Call") == 0  ){
			
			char *annotationFunc = strdup(current->child->annotation); 
			char *annotationFunc2 = strdup(current->child->annotation); 
			
			free(type);
			type  = getTypeLLVM	(current->child->annotation);
			Node temp, tempCast;
			if ( current->child && current->child->brother ){
				gen_body_code(current->child->brother, identation, t);
				temp = current->child->brother;
			}
			else
				temp = NULL;

			tempCast = temp;
			
			char *token;

			token = strtok(annotationFunc, "(");
			
			token = strtok(NULL, ",)");
			while( tempCast ){
				cast(tempCast, token, identation);
				token = strtok(NULL, ",)");
				tempCast = tempCast->brother;
			}
			free(annotationFunc);


			token = strtok(annotationFunc2, "(");			
			token = strtok(NULL, ",)");
			
			ident(identation);
			if ( strcmp(current->annotation, "void") != 0)
				printf("%%%d = ", tempNum);

			printf("call %s @%s(", type, current->child->value);
			while( temp ){
				free(type);
				free(value);

				if ( isTerminal(temp) )
				{
					type = getTypeLLVM(token);
					value = getInitValue(temp->type, temp);
					printf("%s %s", type, value);
					break;
				}
				else
				{
					type = getTypeLLVM(token);
					value = strdup(temp->value);
					printf("%s %%%d", type, temp->tempNum);
					break;
				}
				token = strtok(NULL, ",)");
				temp = temp->brother;
			}
			if ( temp ) 
				temp = temp->brother;
			while( temp ){
				free(type);
				free(value);

				if ( isTerminal(temp) )
				{
					type = getTypeLLVM(token);
					value = getInitValue(temp->type, temp);
					printf(",%s %s", type, value);

				}
				else
				{
					type = getTypeLLVM(token);
					value = strdup(temp->value);
					printf(",%s %%%d", type, temp->tempNum);
				}
				token = strtok(NULL, ",)");
				temp = temp->brother;
			}
			free(annotationFunc2);
			printf(")\n");

			if ( strcmp(current->annotation, "void") != 0)
			{
				current->tempNum = tempNum;
				tempNum++;
			}

		}
		else if ( isTerminal(current) ){
			free(type);
			free(value);
			free(operation);

			type = getTypeLLVM(current->type);
			
			ident(identation);
			if ( t->name == NULL )		// entao é global
				printf("@");
			else
				printf("%%");

			value = getInitValue(current->type, current);

			operation = getOperation(current->annotation, "Add");
			printf("%d = %s %s %s,", tempNum, operation, type, value);

			value = getInitValue(current->type, NULL);
			printf(" %s\n", value);
			
			current->tempNum = tempNum;
			tempNum++;

		}
		else if ( strcmp(current->type, "Id") == 0 ){
			free(type);
			free(align);

			type = getTypeLLVM(current->annotation);
			align = getAlign(current->annotation);

			elementoTabela *e = find_node( current, t );

			ident(identation);
			if ( e )
				printf("%%%d = load %s, %s* %%%s\n", tempNum, type, type, current->value);
			else
				printf("%%%d = load %s, %s* @%s\n", tempNum, type, type, current->value);
			
			current->tempNum = tempNum;
			tempNum++;
			
		}
		else if ( strcmp(current->type, "Return"  ) == 0 ){

			gen_body_code(current->child, identation, t);
			if ( current->child->annotation && strcmp(current->child->annotation, "") !=0 ){
				free(type);
				type = getTypeLLVM(current->child->annotation);
				
				ident(identation);
				printf("ret %s %%%d\n", type, current->child->tempNum);
				
			}
			else{
				ident(identation);
				printf("ret void\n");
			}

			current->tempNum = tempNum;
			tempNum++;

		}


		current = current -> brother;
	}

}

void cast(Node n, char *destType, int identation){
	char *destType_ = strdup(destType);
	char backup1 = n->annotation[0];
	
	n->annotation[0] = toupper(n->annotation[0]);
	destType_[0] = toupper(destType_[0]);
	
	if ( strcmp(n->annotation, destType_) == 0 ){
		n->annotation[0] = backup1;
		free(destType_);
		return;
	}
	
	n->annotation[0] = backup1;
	free(destType_);
	
	if ( strcmp(destType, "double") == 0 || strcmp(destType, "Double") == 0 ){
		ident(identation);
//		extend <result> = sitofp <ty> <value> to <ty2
		printf("%%%d = sitofp %s %%%d to double\n", tempNum, getTypeLLVM(n->annotation),n->tempNum);
		n->tempNum = tempNum;
		tempNum++;

	}
	else if ( strcmp(destType, "short") == 0 || strcmp(destType, "Short") == 0 ){
		// <result> = trunc <ty> <value> to <ty2>
		ident(identation);
		printf("%%%d = trunc %s %%%d to i16\n", tempNum, getTypeLLVM(n->annotation), n->tempNum);
		n->tempNum = tempNum;
		tempNum++;
		return;
	}
	else if ( strcmp(destType, "int") == 0 || strcmp(destType, "char") == 0 || strcmp(destType, "Int") == 0 || strcmp(destType, "Char") == 0){
		/*if atual = double
			trunc
		else if atual = short
			extend
		*/
		if ( strcmp(n->annotation, "double") == 0 || strcmp(n->annotation, "Double") == 0 ){
			//<result> = fptoui <ty> <value> to <ty2>             ; yields ty2
			//current->child->tempNum = tempNum;
			//tempNum++;
			return;
		}
		else if ( strcmp(n->annotation, "short") == 0 || strcmp(n->annotation, "Short") == 0 ){
			ident(identation);
			printf("%%%d = sext i16 %%%d to i32\n", tempNum, n->tempNum);
			n->tempNum = tempNum;
			tempNum++;
		}

	}

}

void ident(int identation){
	for (int i = 0; i< identation; i++)
	{
		printf("\t");
	}
			
}

char *getOperation(char* type, char *op){
	if ( strcmp(op, "Add") == 0 || strcmp(op,"Plus") == 0){
		if ( strcmp(type, "double") == 0 || strcmp(type, "Double") == 0 )
			return strdup("fadd");
		else
			return strdup("add");
	}
	else if ( strcmp(op,"Sub"  ) == 0 || strcmp(op,"Minus") == 0){
		if ( strcmp(type, "double") == 0 || strcmp(type, "Double") == 0 )
			return strdup("fsub");
		else
			return strdup("sub");
	}
	else if ( strcmp(op,"Mul"  ) == 0 ){
		if ( strcmp(type, "double") == 0 || strcmp(type, "Double") == 0 )
			return strdup("fmul");
		else
			return strdup("mul");
	}
	else if ( strcmp(op,"Div"  ) == 0 ){
		if ( strcmp(type, "double") == 0 || strcmp(type, "Double") == 0 )
			return strdup("fdiv");
		else
			return strdup("sdiv");
	}

	else if ( strcmp(op, "Mod") == 0 ){
		if ( strcmp(type, "double") == 0 || strcmp(type, "Double") == 0 )
			return strdup("frem");
		else
			return strdup("srem");
	}
	
	else if ( strcmp(op, "Eq" ) == 0 ){
		if ( strcmp(type, "double") == 0 || strcmp(type, "Double") == 0 )
			return strdup("fcmp oeq");
		else
			return strdup("icmp eq");
	}
	
	else if ( strcmp(op, "Ne" ) == 0 ){
		if ( strcmp(type, "double") == 0 || strcmp(type, "Double") == 0 )
			return strdup("fcmp one");
		else
			return strdup("icmp ne");

	}
	
	else if ( strcmp(op, "Le" ) == 0 ){
		if ( strcmp(type, "double") == 0 || strcmp(type, "Double") == 0 )
			return strdup("fcmp ole");
		else
			return strdup("icmp sle");
	}
	
	else if ( strcmp(op, "Ge" ) == 0 ){
		if ( strcmp(type, "double") == 0 || strcmp(type, "Double") == 0 )
			return strdup("fcmp oge");
		else
			return strdup("icmp sge");
	}
	
	else if ( strcmp(op, "Lt" ) == 0 ){
		if ( strcmp(type, "double") == 0 || strcmp(type, "Double") == 0 )
			return strdup("fcmp olt");
		else
			return strdup("icmp slt");
	}
	
	else if ( strcmp(op, "Gt" ) == 0 ){
		if ( strcmp(type, "double") == 0 || strcmp(type, "Double") == 0 )
			return strdup("fcmp ogt");
		else
			return strdup("icmp sgt");
	}
	
	else if ( strcmp(op, "Or" ) == 0  ){		// tratado in loco
		return strdup("TODO");
	}
	
	else if ( strcmp(op, "And") == 0  ){		// tratado in loco
		return strdup("TODO");
	}
	
	else if ( strcmp(op, "Not") == 0 ){			// tratado in loco
		return strdup("TODO");
	}
			
	else if ( strcmp(op, "BitWiseAnd") == 0  ){
		return strdup("and");
	}
	
	else if ( strcmp(op, "BitWiseOr" ) == 0  ){
		return strdup("or");
	}
	
	else if ( strcmp(op, "BitWiseXor") == 0  ){
		return strdup("xor");
	}
	
	else
		return strdup("TODO");
}

int isBooleanOperator (char *type){

	return  strcmp(type, "Eq" ) == 0 ||
			strcmp(type, "Ne" ) == 0 ||  				// booleans
			strcmp(type, "Le" ) == 0 ||
			strcmp(type, "Ge" ) == 0 ||
			strcmp(type, "Lt" ) == 0 ||
			strcmp(type, "Gt" ) == 0 ||
			strcmp(type, "And") == 0 ||
			strcmp(type, "Or" ) == 0 ;

}