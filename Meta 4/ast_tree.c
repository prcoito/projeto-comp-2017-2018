#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ast_tree.h"

//create node
Node create_node(char* nodeType, char* value, int numLinha, int numColuna){
    Node temp = (Node)malloc(sizeof(node));
    temp->child = NULL;
    temp->brother = NULL;
    temp->type = (char*)malloc((strlen(nodeType)+1)*sizeof(char));
    strcpy(temp->type, nodeType);
    temp->value = (char*)malloc((strlen(value)+1)*sizeof(char));
    strcpy(temp->value, value);

    temp->numLinhas = numLinha;
    temp->numColunas = numColuna;
    temp->annotation = strdup("");

    return temp;
}

//add node
void add_child(Node parent, Node child){
    if(child==NULL)
        return;

    Node temp;
    if(parent->child != NULL){
        temp = parent->child;
        while(temp->brother != NULL)
            temp = temp->brother;
        temp->brother = child;
    }
    else
        parent->child = child;

}

//add brother
void add_brother(Node current, Node new_bro){
    Node temp = current;
    while(temp->brother != NULL)
        temp = temp->brother;
    temp->brother = new_bro;
}

//clear tree
void free_tree(Node current){
    if(current == NULL){
        return;
    }
    Node temp = current;

    free(temp->type);
    free(temp->value);
    free(temp->annotation);

    if(temp->child != NULL)
        free_tree(temp->child);
    if(temp->brother != NULL){
        free_tree(temp->brother);
    }
    free(temp);
}

//print tree
void print_tree(Node current, int depth){
    int a;
    Node temp = current;

    for(a = 1; a < depth; a++){
        printf("..");
    }

    if(strcmp(current->value, "") != 0){
        printf("%s(%s)", current->type, current->value);
    }
    else{
        printf("%s", current->type);
    }

    if(strcmp(current->annotation, "") != 0)
        printf(" - %s", current->annotation);

    printf("\n");

    if(temp->child != NULL)
        print_tree(temp->child, depth+1);
    if(temp->brother != NULL){
        print_tree(temp->brother, depth);
    }
}
